import {
  ACTION_VENUE_SET,
  ACTION_VENUE_FETCH,
  ACTION_VENUE_POST,
  ACTION_VENUE_SUCCESS,
  ACTION_VENUE_ERROR,
} from '../actions/venueActions';

const initialState = {
  venues: [],
  fetching: false,
  posting: false,
};

export default function venueReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_VENUE_SET:
      return {
        venues: action.payload,
        fetching: false,
        posting: false,
      };

    case ACTION_VENUE_FETCH:
      return {
        ...state,
        venues: [],
        fetching: true,
      };

    case ACTION_VENUE_POST:
      return {
        ...state,
        posting: true,
      };

    case ACTION_VENUE_SUCCESS:
      return {
        ...state,
        posting: false,
      };

    case ACTION_VENUE_ERROR:
      return {
        ...state,
        fetching: false,
        posting: false,
      };

    default:
      return state;
  }
}
