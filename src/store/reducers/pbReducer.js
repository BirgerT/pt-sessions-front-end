import {
  ACTION_PB_SET,
  ACTION_PB_FETCH,
  ACTION_PB_POST,
  ACTION_PB_SUCCESS,
  ACTION_PB_ERROR,
} from '../actions/pbActions';

const initialState = {
  pbs: [],
  fetching: false,
  posting: false,
};

export default function pbReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_PB_SET:
      return {
        pbs: action.payload,
        fetching: false,
        posting: false,
      };

    case ACTION_PB_FETCH:
      return {
        ...state,
        pbs: [],
        fetching: true,
      };

    case ACTION_PB_POST:
      return {
        ...state,
        posting: true,
      };

    case ACTION_PB_SUCCESS:
      return {
        ...state,
        posting: false,
      };

    case ACTION_PB_ERROR:
      return {
        ...state,
        fetching: false,
        posting: false,
      };

    default:
      return state;
  }
}
