import {
  ACTION_WORKOUT_SET,
  ACTION_WORKOUT_FETCH,
  ACTION_WORKOUT_POST,
  ACTION_WORKOUT_UPDATE,
  ACTION_WORKOUT_SUCCESS,
  ACTION_WORKOUT_ERROR,
} from '../actions/workoutActions';

const initialState = {
  workouts: [],
  fetching: false,
  posting: false,
};

export default function workoutReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_WORKOUT_SET:
      return {
        workouts: action.payload,
        fetching: false,
        posting: false,
      };

    case ACTION_WORKOUT_FETCH:
      return {
        ...state,
        workouts: [],
        fetching: true,
      };

    case ACTION_WORKOUT_POST:
      return {
        ...state,
        posting: true,
      };

    case ACTION_WORKOUT_UPDATE:
      const newWorkout = action.payload;
      const workouts = state.workouts.map((workout) => {
        return newWorkout.id === workout.id ? newWorkout : workout;
      });
      return {
        ...state,
        workouts,
        fetching: false,
      };

    case ACTION_WORKOUT_SUCCESS:
      return {
        ...state,
        posting: false,
      };

    case ACTION_WORKOUT_ERROR:
      return {
        ...state,
        fetching: false,
        posting: false,
      };

    default:
      return state;
  }
}
