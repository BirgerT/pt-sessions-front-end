import {
  ACTION_EXERCISE_SET,
  ACTION_EXERCISE_FETCH,
  ACTION_EXERCISE_POST,
  ACTION_EXERCISE_SUCCESS,
  ACTION_EXERCISE_ERROR,
} from '../actions/exerciseActions';

const initialState = {
  exercises: [],
  fetching: false,
  posting: false,
};

export default function exerciseReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_EXERCISE_SET:
      return {
        exercises: action.payload,
        fetching: false,
        posting: false,
      };

    case ACTION_EXERCISE_FETCH:
      return {
        ...state,
        exercises: [],
        fetching: true,
      };

    case ACTION_EXERCISE_POST:
      return {
        ...state,
        posting: true,
      };

    case ACTION_EXERCISE_SUCCESS:
      return {
        ...state,
        posting: false,
      };

    case ACTION_EXERCISE_ERROR:
      return {
        ...state,
        fetching: false,
        posting: false,
      };

    default:
      return state;
  }
}
