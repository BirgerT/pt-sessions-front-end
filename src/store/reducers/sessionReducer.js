import {
  ACTION_SESSION_SET,
  ACTION_SESSION_FETCH,
  ACTION_SESSION_POST,
  ACTION_SESSION_SUCCESS,
  ACTION_SESSION_ERROR,
} from '../actions/sessionActions';

const initialState = {
  sessions: [],
  fetching: false,
  posting: false,
};

export default function sessionReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_SESSION_SET:
      return {
        sessions: action.payload,
        fetching: false,
        posting: false,
      };

    case ACTION_SESSION_FETCH:
      return {
        ...state,
        sessions: [],
        fetching: true,
      };

    case ACTION_SESSION_POST:
      return {
        ...state,
        posting: true,
      };

    case ACTION_SESSION_SUCCESS:
      return {
        ...state,
        posting: false,
      };

    case ACTION_SESSION_ERROR:
      return {
        ...state,
        fetching: false,
        posting: false,
      };

    default:
      return state;
  }
}
