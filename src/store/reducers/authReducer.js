import { ACTION_AUTH_LOGIN, ACTION_AUTH_LOGOUT } from '../actions/authActions';

const initialState = {
  user: '',
  userRole: '',
  trainerId: 0,
  userName: '',
  isAuthenticated: false,
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_AUTH_LOGIN:
      return {
        user: action.payload.token,
        userRole: action.payload.role,
        trainerId: action.payload.trainerId,
        userName: action.payload.name,
        isAuthenticated: true,
      };

    case ACTION_AUTH_LOGOUT:
      return initialState;

    default:
      return state;
  }
}
