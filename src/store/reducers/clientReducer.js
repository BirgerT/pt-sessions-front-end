import {
  ACTION_CLIENT_SET,
  ACTION_CLIENT_FETCH,
  ACTION_CLIENT_POST,
  ACTION_CLIENT_SUCCESS,
  ACTION_CLIENT_ERROR,
} from '../actions/clientActions';

const initialState = {
  clients: [],
  fetching: false,
  posting: false,
};

export default function clientReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_CLIENT_SET:
      return {
        clients: action.payload,
        fetching: false,
        posting: false,
      };

    case ACTION_CLIENT_FETCH:
      return {
        ...state,
        clients: [],
        fetching: true,
      };
    case ACTION_CLIENT_POST:
      return {
        ...state,
        posting: true,
      };

    case ACTION_CLIENT_SUCCESS:
      return {
        ...state,
        posting: false,
      };

    case ACTION_CLIENT_ERROR:
      return {
        ...state,
        fetching: false,
        posting: false,
      };

    default:
      return state;
  }
}
