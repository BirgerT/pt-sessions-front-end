import {
  ACTION_PROGRAM_SET,
  ACTION_PROGRAM_FETCH,
  ACTION_PROGRAM_POST,
  ACTION_PROGRAM_SUCCESS,
  ACTION_PROGRAM_ERROR,
} from '../actions/programActions';

const initialState = {
  programs: [],
  fetching: false,
  posting: false,
};

export default function programReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_PROGRAM_SET:
      return {
        programs: action.payload,
        fetching: false,
        posting: false,
      };

    case ACTION_PROGRAM_FETCH:
      return {
        ...state,
        programs: [],
        fetching: true,
      };

    case ACTION_PROGRAM_POST:
      return {
        ...state,
        posting: true,
      };

    case ACTION_PROGRAM_SUCCESS:
      return {
        ...state,
        posting: false,
      };

    case ACTION_PROGRAM_ERROR:
      return {
        ...state,
        fetching: false,
        posting: false,
      };

    default:
      return state;
  }
}
