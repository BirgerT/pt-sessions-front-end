import { combineReducers } from 'redux';
import auth from './authReducer';
import workoutReducer from './workoutReducer';
import venueReducer from './venueReducer';
import clientReducer from './clientReducer';
import exerciseReducer from './exerciseReducer';
import sessionReducer from './sessionReducer';
import programReducer from './programReducer';
import pbReducer from './pbReducer';

export const rootReducers = combineReducers({
  auth,
  workoutReducer,
  venueReducer,
  clientReducer,
  exerciseReducer,
  sessionReducer,
  programReducer,
  pbReducer,
});
