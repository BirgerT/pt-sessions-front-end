import axios from 'axios';
import { PT_SESS_API_BASE_URL } from '../../config.json';
import config from '../../config.json';

export const ACTION_VENUE_SET = 'venue:SET';
export const ACTION_VENUE_FETCH = 'venue:FETCH';
export const ACTION_VENUE_ERROR = 'venue:ERROR';
export const ACTION_VENUE_POST = 'venue:POST';
export const ACTION_VENUE_SUCCESS = 'venue:SUCCESS';

export const venuesGetAction = (token, onFailure) => async (dispatch) => {
  dispatch({
    type: ACTION_VENUE_FETCH,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await axios.get(
      config.PT_SESS_API_BASE_URL + 'venues',
      cfg
    );

    dispatch({
      type: ACTION_VENUE_SET,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: ACTION_VENUE_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const venuePostAction = (venue, token, onSuccess, onFailure) => async (
  dispatch
) => {
  dispatch({
    type: ACTION_VENUE_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.post(PT_SESS_API_BASE_URL + 'venues', venue, cfg);

    dispatch({
      type: ACTION_VENUE_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_VENUE_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const venuesPutAction = (venue, token, onSuccess, onFailure) => async (
  dispatch
) => {
  // Map typeId to match API object.
  venue.typeId = venue.type.id;

  dispatch({
    type: ACTION_VENUE_POST,
  });
  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.put(PT_SESS_API_BASE_URL + 'venues/' + venue.id, venue, cfg);

    dispatch({
      type: ACTION_VENUE_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_VENUE_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};
