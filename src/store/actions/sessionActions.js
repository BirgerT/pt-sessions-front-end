import axios from 'axios';
import { PT_SESS_API_BASE_URL } from '../../config.json';

export const ACTION_SESSION_POST = 'session:POST';
export const ACTION_SESSION_SUCCESS = 'session:SUCCESS';
export const ACTION_SESSION_ERROR = 'session:ERROR';
export const ACTION_SESSION_SET = 'session:SET';
export const ACTION_SESSION_FETCH = 'session:FETCH';

export const sessionsGetAction = (token, onFailure) => async (dispatch) => {
  dispatch({
    type: ACTION_SESSION_FETCH,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await axios.get(PT_SESS_API_BASE_URL + 'sessions', cfg);

    dispatch({
      type: ACTION_SESSION_SET,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: ACTION_SESSION_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const sessionsPostAction = (
  session,
  token,
  onSuccess,
  onFailure
) => async (dispatch) => {
  dispatch({
    type: ACTION_SESSION_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.post(PT_SESS_API_BASE_URL + 'sessions', session, cfg);

    dispatch({
      type: ACTION_SESSION_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_SESSION_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const sessionsPutAction = (
  session,
  token,
  onSuccess,
  onFailure
) => async (dispatch) => {
  dispatch({
    type: ACTION_SESSION_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.put(
      PT_SESS_API_BASE_URL + 'sessions/' + session.id,
      session,
      cfg
    );

    dispatch({
      type: ACTION_SESSION_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_SESSION_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};
