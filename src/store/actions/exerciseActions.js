import axios from 'axios';
import { PT_SESS_API_BASE_URL } from '../../config.json';

export const ACTION_EXERCISE_SET = 'exercise:SET';
export const ACTION_EXERCISE_FETCH = 'exercise:FETCH';
export const ACTION_EXERCISE_POST = 'exercise:POST';
export const ACTION_EXERCISE_SUCCESS = 'exercise:SUCCESS';
export const ACTION_EXERCISE_ERROR = 'exercise:ERROR';

export const exercisesGetAction = (token, onFailure) => async (dispatch) => {
  dispatch({
    type: ACTION_EXERCISE_FETCH,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await axios.get(PT_SESS_API_BASE_URL + 'exercises', cfg);

    dispatch({
      type: ACTION_EXERCISE_SET,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: ACTION_EXERCISE_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const exercisePostAction = (
  exercise,
  token,
  onSuccess,
  onFailure
) => async (dispatch) => {
  dispatch({
    type: ACTION_EXERCISE_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.post(PT_SESS_API_BASE_URL + 'exercises', exercise, cfg);

    dispatch({
      type: ACTION_EXERCISE_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_EXERCISE_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const exercisePutAction = (
  exercise,
  token,
  onSuccess,
  onFailure
) => async (dispatch) => {
  dispatch({
    type: ACTION_EXERCISE_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.put(
      PT_SESS_API_BASE_URL + 'exercises/' + exercise.id,
      exercise,
      cfg
    );

    dispatch({
      type: ACTION_EXERCISE_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_EXERCISE_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};
