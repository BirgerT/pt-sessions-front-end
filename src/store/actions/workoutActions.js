import axios from 'axios';
import { PT_SESS_API_BASE_URL } from '../../config.json';

export const ACTION_WORKOUT_SET = 'workout:SET';
export const ACTION_WORKOUT_FETCH = 'workout:FETCH';
export const ACTION_WORKOUT_POST = 'workout:POST';
export const ACTION_WORKOUT_UPDATE = 'workout:UPDATE';
export const ACTION_WORKOUT_SUCCESS = 'workout:SUCCESS';
export const ACTION_WORKOUT_ERROR = 'workout:ERROR';

export const workoutsGetAction = (token, onFailure) => async (dispatch) => {
  dispatch({
    type: ACTION_WORKOUT_FETCH,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await axios.get(PT_SESS_API_BASE_URL + 'workouts', cfg);

    dispatch({
      type: ACTION_WORKOUT_SET,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: ACTION_WORKOUT_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const workoutPostAction = (
  workout,
  token,
  onSuccess,
  onFailure
) => async (dispatch) => {
  dispatch({
    type: ACTION_WORKOUT_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.post(PT_SESS_API_BASE_URL + 'workouts', workout, cfg);

    dispatch({
      type: ACTION_WORKOUT_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_WORKOUT_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const workoutPutAction = (
  workout,
  token,
  onSuccess,
  onFailure
) => async (dispatch) => {
  dispatch({
    type: ACTION_WORKOUT_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.put(
      PT_SESS_API_BASE_URL + 'workouts/' + workout.id,
      workout,
      cfg
    );

    dispatch({
      type: ACTION_WORKOUT_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_WORKOUT_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const workoutExercisesGetAction = (workout, token, onFailure) => async (
  dispatch
) => {
  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await axios.get(
      PT_SESS_API_BASE_URL + 'workouts/' + workout.id + '/exercises/',
      cfg
    );

    dispatch({
      type: ACTION_WORKOUT_UPDATE,
      payload: {
        ...workout,
        exercises: response.data,
      },
    });
  } catch (error) {
    dispatch({
      type: ACTION_WORKOUT_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};
