import axios from 'axios';
import { PT_SESS_API_BASE_URL } from '../../config.json';

export const ACTION_PB_POST = 'pb:POST';
export const ACTION_PB_SUCCESS = 'pb:SUCCESS';
export const ACTION_PB_ERROR = 'pb:ERROR';
export const ACTION_PB_SET = 'pb:SET';
export const ACTION_PB_FETCH = 'pb:FETCH';

export const pbsGetAction = (clientId, token, onFailure) => async (
  dispatch
) => {
  dispatch({
    type: ACTION_PB_FETCH,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await axios.get(
      `${PT_SESS_API_BASE_URL}clients/${clientId}/personal-bests`,
      cfg
    );

    dispatch({
      type: ACTION_PB_SET,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: ACTION_PB_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const pbPostAction = (
  pb,
  clientId,
  token,
  onSuccess,
  onFailure
) => async (dispatch) => {
  dispatch({
    type: ACTION_PB_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.post(
      `${PT_SESS_API_BASE_URL}clients/${clientId}/personal-bests`,
      pb,
      cfg
    );

    dispatch({
      type: ACTION_PB_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_PB_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const pbPutAction = (pb, token, onSuccess, onFailure) => async (
  dispatch
) => {
  const cfg = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  dispatch({
    type: ACTION_PB_POST,
  });

  try {
    await axios.put(PT_SESS_API_BASE_URL + 'pbs/' + pb.id, pb, cfg);

    dispatch({
      type: ACTION_PB_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_PB_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};
