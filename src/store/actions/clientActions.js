import axios from 'axios';
import { PT_SESS_API_BASE_URL } from '../../config.json';

export const ACTION_CLIENT_POST = 'client:POST';
export const ACTION_CLIENT_SUCCESS = 'client:SUCCESS';
export const ACTION_CLIENT_ERROR = 'client:ERROR';
export const ACTION_CLIENT_SET = 'client:SET';
export const ACTION_CLIENT_FETCH = 'client:FETCH';

export const clientsGetAction = (token, onFailure) => async (dispatch) => {
  dispatch({
    type: ACTION_CLIENT_FETCH,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await axios.get(PT_SESS_API_BASE_URL + 'clients', cfg);

    dispatch({
      type: ACTION_CLIENT_SET,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: ACTION_CLIENT_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const clientPostAction = (client, token, onSuccess, onFailure) => async (
  dispatch
) => {
  dispatch({
    type: ACTION_CLIENT_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.post(PT_SESS_API_BASE_URL + 'clients', client, cfg);

    dispatch({
      type: ACTION_CLIENT_SUCCESS,
    });

    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_CLIENT_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const clientsPutAction = (client, token, onSuccess, onFailure) => async (
  dispatch
) => {
  dispatch({
    type: ACTION_CLIENT_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.put(PT_SESS_API_BASE_URL + 'clients/' + client.id, client, cfg);

    dispatch({
      type: ACTION_CLIENT_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_CLIENT_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};
