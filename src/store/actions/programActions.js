import axios from 'axios';
import { PT_SESS_API_BASE_URL } from '../../config.json';

export const ACTION_PROGRAM_SET = 'program:SET';
export const ACTION_PROGRAM_FETCH = 'program:FETCH';
export const ACTION_PROGRAM_POST = 'program:POST';
export const ACTION_PROGRAM_SUCCESS = 'program:SUCCESS';
export const ACTION_PROGRAM_ERROR = 'program:ERROR';

export const programsGetAction = (token, onFailure) => async (dispatch) => {
  dispatch({
    type: ACTION_PROGRAM_FETCH,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await axios.get(PT_SESS_API_BASE_URL + 'programs', cfg);

    dispatch({
      type: ACTION_PROGRAM_SET,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: ACTION_PROGRAM_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const programsPostAction = (
  program,
  token,
  onSuccess,
  onFailure
) => async (dispatch) => {
  dispatch({
    type: ACTION_PROGRAM_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.post(PT_SESS_API_BASE_URL + 'programs', program, cfg);

    dispatch({
      type: ACTION_PROGRAM_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_PROGRAM_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};

export const programsPutAction = (
  program,
  token,
  onSuccess,
  onFailure
) => async (dispatch) => {
  dispatch({
    type: ACTION_PROGRAM_POST,
  });

  try {
    const cfg = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    await axios.put(
      PT_SESS_API_BASE_URL + 'programs/' + program.id,
      program,
      cfg
    );

    dispatch({
      type: ACTION_PROGRAM_SUCCESS,
    });
    if (onSuccess) onSuccess();
  } catch (error) {
    dispatch({
      type: ACTION_PROGRAM_ERROR,
    });
    if (onFailure) onFailure(error);
  }
};
