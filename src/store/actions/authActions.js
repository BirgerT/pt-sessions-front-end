import jwt from 'jsonwebtoken';

export const ACTION_AUTH_LOGIN = 'auth:LOGIN';
export const ACTION_AUTH_LOGOUT = 'auth:LOGOUT';

const AUTH_TOKEN_KEY = '_ptsess--user-token';

export function login(token) {
  const decodedToken = jwt.decode(token, { complete: true });
  const role = decodedToken.payload.role;
  const trainerId = decodedToken.payload.tid;
  const name = decodedToken.payload.name;

  sessionStorage.setItem(AUTH_TOKEN_KEY, token);

  return (dispatch) => {
    dispatch({
      type: ACTION_AUTH_LOGIN,
      payload: { token, role, trainerId, name },
    });
  };
}

export function logout() {
  sessionStorage.removeItem(AUTH_TOKEN_KEY);

  return (dispatch) => {
    dispatch({
      type: ACTION_AUTH_LOGOUT,
    });
  };
}
