// Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Image from 'react-bootstrap/Image';
import { connect } from 'react-redux';

// Images
import dumbbell from '../../assets/img/dumbbell.png';

// Style
import './Header.scss';

function Header(props) {
  if (!props.auth.isAuthenticated)
    return null;
  
  return (
    <Navbar bg="primary" variant="dark" expand="lg">
      <Container>
        <Navbar.Brand as={NavLink} to="/">
          <Image src={dumbbell} id="header--img-dumbbell" />
          PT Sessions
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="header--nav" />
        <Navbar.Collapse id="header--nav" className="mr-auto">
          <Nav>
            <Nav.Link as={NavLink} to="/" exact>Dashboard</Nav.Link>
            <Nav.Link as={NavLink} to="/sessions">Sessions</Nav.Link>
            <Nav.Link as={NavLink} to="/programs">Programs</Nav.Link>
            <Nav.Link as={NavLink} to="/workouts">Workouts</Nav.Link>
            <Nav.Link as={NavLink} to="/exercises">Exercises</Nav.Link>
            <Nav.Link as={NavLink} to="/clients">Clients</Nav.Link>
            <Nav.Link as={NavLink} to="/venues">Venues</Nav.Link>
            <Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps, null)(Header);
