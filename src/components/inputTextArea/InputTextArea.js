import React from 'react';
import Form from 'react-bootstrap/Form'
import {useState} from 'react'
import './InputTextArea.scss'

const InputTextArea = (props) => {
    
    const [inputType] = useState(props.type)
    const [rows] = useState(props.rows)
    const [inputPlaceholder] = useState(props.placeholder)
    const [inputLabel] = useState(props.label)
    const [inputValue, setInputValue] = useState(props.value)

    function handleChange(event){
        setInputValue(event.target.value);
        
        // The onChange is used differently in some places.
        if (props.onChange && props.eventOnChange) {
            props.onChange(event);
        } else if (props.onChange) {
            props.onChange(event.target.value);
        }
    }

  return (
    <div className="inputField">
        <Form.Label className="inputField-label">{inputLabel}</Form.Label>
        <Form.Control
            name={props.name}
            readOnly={props.disabled}
            type={inputType}
            value={inputValue} 
            placeholder={inputPlaceholder} 
            onChange={handleChange} 
            className="inputField-textArea"
            as="textarea"
            rows={rows}
        />
    </div>
    );
}

export default InputTextArea;