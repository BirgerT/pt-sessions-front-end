import { Toast } from 'react-bootstrap';

const SubmitToast = ({ message, show, setShow }) => {
  return (
    <Toast
      style={{
        position: 'fixed',
        bottom: 30,
        left: '50%',
      }}
      onClose={() => setShow(false)}
      show={show}
      delay={2000}
      autohide
    >
      <Toast.Header>
      </Toast.Header>
      <Toast.Body>{message}</Toast.Body>
    </Toast>
  );
};

export default SubmitToast;
