import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import { GoPlus } from 'react-icons/go';
import { Link } from 'react-router-dom';
import './BaseButton.scss';

const BaseButton = (props) => {
  const [text] = useState(props.text);
  const [link] = useState(props.link);
  const [type] = useState(props.type);
  const [variant] = useState(props.variant);

  // Links don't like being disabled. 
  let linkProps = {}
  if (link) {
      linkProps = {as: Link, to: link};
  }

  return (
    <>
      <Button
        className="base-button"
        type={type}
        variant={type === 'add' ? 'success' : variant}
        disabled={props.disabled}
        onClick={props.onClick}
        {...linkProps}
      >
        {type === 'add' && <GoPlus className="base-button--pluss-sign" />}
        {text}
      </Button>
    </>
  );
};
export default BaseButton;
