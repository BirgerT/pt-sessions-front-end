import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { TokenHasExpired } from '../../../services/authService';
import { login } from '../../../store/actions/authActions';

const withAuth = (WrappedComponent, authRole) => {
    const Component = props => {
        // If not authed in redux store, check session
        if (!props.auth.isAuthenticated) {
          const userToken = sessionStorage.getItem('_ptsess--user-token');
          if (!userToken) {
            return <Redirect to='/login' />
          }
          else if (TokenHasExpired(userToken)) {
            console.log('bounced. why?', userToken, TokenHasExpired(userToken))
            return <Redirect to='/login' />
          }

          // Token looks good. Login with it
          props.login(userToken);
        }
            
        
        // If the user role doesn't match auth role required, bounce.
        // Backend would reject, anyway, also.
        if (authRole !== undefined && authRole !== props.auth.userRole)
            return <Redirect to='/login' />
        
        // All good!
        return <WrappedComponent {...props} />
    };

    const mapDispatchToProps = dispatch => {
      return {
        login: token => {
          dispatch(login(token));
        }
      }
    };

    const mapStateToProps = state => {
        return {
            auth: state.auth
        };
    };
    
    return connect(mapStateToProps, mapDispatchToProps)(Component);
}

export default withAuth;