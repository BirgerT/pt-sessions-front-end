import React, {useState} from 'react';
import Card from 'react-bootstrap/Card'
import { Link } from 'react-router-dom';
import './IconCard.scss'
 
const IconCard = (props) => {
    const [name] = useState(props.name)
    const [link] = useState(props.link)

    return (
    <>
        <Card className="dashboard-card">
            <Link to={link}>
            <Card.Body>
                <img src={require(`../../assets/img/${name}.png`).default} alt={name} className="dashboard-card-image"/>
                <Card.Title className="dashboard-card-title"><p>{name}</p></Card.Title>
            </Card.Body>
            </Link>
        </Card>
    </>
    )
};
export default IconCard;