import React from 'react';
import Select from 'react-select';
import './CustomSelect.scss';

const CustomSelect = ({
  label,
  id,
  options,
  selectedOption,
  setSelectedOption,
  onChange,
  disabled,
}) => {
  return (
    <div className='customSelect' id={id}>
      <label className='customSelect-label'>{label}</label>
      <Select
        className='customSelect-input'
        options={options}
        value={selectedOption}
        onChange={onChange ?? setSelectedOption} // <- Renaming setSelectedOption for consistency
        isDisabled={disabled}
        menuPortalTarget={document.querySelector('body')}
      />
    </div>
  );
};

export default CustomSelect;
