import './FloatRight.scss';

function FloatRight(props) {
  return <div className="float-right--container">{props.children}</div>;
}
export default FloatRight;
