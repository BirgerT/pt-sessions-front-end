import { useEffect, useMemo, useState } from 'react';

// Components
import { Button } from 'react-bootstrap';
import DataTable, { createTheme } from 'react-data-table-component';
import ExpandedComponent from './ExpandedComponent';

// Style
import './infoTable.scss';
import variables from '../../utils/_variables.scss';

const FilterComponent = ({ filterText, onFilter, onClear }) => (
  <>
    <input
      className='filter-input'
      id='search'
      type='text'
      placeholder='Filter'
      aria-label='Search Input'
      value={filterText}
      onChange={onFilter}
    />
    <Button id='clear-btn' onClick={onClear}>
      X
    </Button>
  </>
);

const InfoTable = ({
  columns,
  data,
  withFilter,
  onEdit,
  disableExpandableRows,
  expandedDetails,
}) => {
  const [tableData, setTableData] = useState(data);
  const [filterText, setFilterText] = useState('');
  const [resetPaginationToggle, setResetPaginationToggle] = useState(false);

  // Update table data whenever new data arrives.
  useEffect(() => {
    setTableData(data);
  }, [data]);

  const filteredItems = tableData.filter((item) =>
    Object.keys(item).some((k) =>
      item[k].toString().toLowerCase().includes(filterText.toLowerCase())
    )
  );

  const subHeaderComponentMemo = useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText('');
      }
    };

    return (
      <FilterComponent
        onFilter={(e) => setFilterText(e.target.value)}
        onClear={handleClear}
        filterText={filterText}
      />
    );
  }, [filterText, resetPaginationToggle]);

  const filterVisible = withFilter ? subHeaderComponentMemo : [];

  // Set the same background as the rest of the page.
  createTheme('pt-sessions-light', {
    background: {
      default: variables.background,
    },
  });

  // Save the update to table data and pass it along to the parent.
  const handleSave = (details) => {
    setTableData(
      tableData.map((row) => {
        return details.id === row.id ? details : row;
      })
    );
    // Handle put requests in the parent.
    onEdit(details);
  };

  return (
    <DataTable
      columns={columns}
      data={filteredItems}
      pagination
      striped
      highlightOnHover
      paginationResetDefaultPage={resetPaginationToggle}
      noHeader
      expandableRows={!disableExpandableRows}
      expandOnRowClicked
      expandableRowsComponent={
        <ExpandedComponent
          data={data}
          onSave={handleSave}
          expandedDetails={expandedDetails}
        />
      }
      expandableRowDisabled={() => !!disableExpandableRows}
      subHeader
      subHeaderComponent={filterVisible}
      theme='pt-sessions-light'
    />
  );
};

export default InfoTable;
