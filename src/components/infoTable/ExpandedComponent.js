import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { GoPencil } from 'react-icons/go';

const ExpandedComponent = ({ data, onSave, expandedDetails }) => {
  const [disabled, setDisabled] = useState(true);
  const [details, setDetails] = useState(data);

  useEffect(() => {
    setDetails(data);
  }, [data]);

  // Enable editing
  const OnEditClick = () => {
    setDisabled(false);
  };

  // Cancel editing
  const OnCancelClick = () => {
    setDetails(data);
    setDisabled(true);
  };

  // Save edits
  const OnSaveClick = () => {
    setDisabled(true);
    onSave(details);
  };

  // Update details when something is typed.
  const onChange = (e) => {
    if (e.target) {
      // If the update comes from a simple component.
      setDetails({
        ...details,
        [e.target.name]: e.target.value.trim(),
      });
    } else {
      // If the update is defined by the developer.
      setDetails({
        ...details,
        [e.name]: e.value,
      });
    }
  };

  return (
    <>
      {React.cloneElement(expandedDetails, { data, disabled, onChange })}
      <div className='expanded-component--btn-container'>
        {disabled && (
          <Button className='expanded-component--edit-btn' variant='primary' onClick={OnEditClick}>
            <GoPencil />
            Edit
          </Button>
        )}
        {!disabled && (
          <>
            <Button
              className='expanded-component--save-btn'
              variant='success'
              onClick={OnSaveClick}
            >
              Save
            </Button>

            <Button
              className='expanded-component--cancel-btn'
              variant='cancel'
              onClick={OnCancelClick}
            >
              Cancel
            </Button>
          </>
        )}
      </div>
    </>
  );
};

export default ExpandedComponent;
