import React, { useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import { useState } from 'react';
import './InputField.scss';

const InputField = (props) => {
  const [inputType] = useState(props.type);
  const [inputPlaceholder] = useState(props.placeholder);
  const [inputLabel] = useState(props.label);
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    setInputValue(props.value);
  }, [props.value]);

  function handleChange(event) {
    setInputValue(event.target.value);

    // The onChange is used differently in some places.
    if (props.onChange && props.eventOnChange) {
      props.onChange(event);
    } else if (props.onChange) {
      props.onChange(event.target.value);
    }
  }

  return (
    <div className='inputField'>
      <Form.Label className='inputField-label'>{inputLabel}</Form.Label>
      <Form.Control
        readOnly={props.disabled}
        type={inputType}
        value={inputValue ?? ''}
        placeholder={inputPlaceholder}
        onChange={handleChange}
        name={props.name}
        className='inputField-input'
      />
    </div>
  );
};

export default InputField;
