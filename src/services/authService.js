import jwt from 'jsonwebtoken';

export function TokenHasExpired(token) {
    const decodedToken = jwt.decode(token);
    if (!decodedToken) return false;
    const dateNow = new Date();
    if (decodedToken.exp < dateNow.getTime() / 1000) return true;
    else return false;
}