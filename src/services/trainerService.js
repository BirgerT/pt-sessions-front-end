import config from '../config.json';
import axios from 'axios';

async function getAll() {
    const options = {
        url: config.PT_SESS_API_BASE_URL + 'trainers',
        method: 'GET'
    };

    var resp = await axios(options);
    if (resp.statusText !== "OK") {
        // Throw some horrible exception here. TODO
    }
    return resp.data;
}

const _exports = { getAll };
export default _exports;