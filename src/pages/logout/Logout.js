import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { logout } from '../../store/actions/authActions';

const Logout = props => {
  props.logout();
  
  return (
    <Redirect to='/login' />
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  };
};
  
const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => {
      dispatch(logout());
    }
  }
};
  
export default connect(mapStateToProps, mapDispatchToProps)(Logout);