import React from 'react';
import withAuth from '../../components/HOCs/auth/WithAuth';

const AuthTest = props => {

    const getGloriousData = () => {
        const token = props.auth.user;
        console.log('And the token is...', token);

        const options = {
            method: 'GET',
            headers: {
                Authorization : `Bearer ${token}`
            }
        }

        fetch('https://localhost:44355/api/v1/auth/google', options)
            .then(resp => resp.json())
            .then(resp => console.log('HIGHLY SECURE DATA: ', resp));
    }
    return (
        <>
          <div>I'm behind a frontend auth wall.</div>
          <button onClick={getGloriousData}>Click me to get data behind (backend) auth wall!</button>
        </>
    )
}

export default withAuth(AuthTest);

