import React, { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form';
import './AddNewWorkout.scss';

const InputFieldExercise = ({
  name,
  label,
  value,
  type,
  placeholder,
  onChange,
  disabled,
}) => {
  const [fieldValue, setFieldValue] = useState(value);

  useEffect(() => {
    setFieldValue(value)
  }, [value])

  function handleChange(event) {
    setFieldValue(event.target.value);
    if (onChange) onChange(event);
  }

  return (
    <div className='inputField-exercise'>
      <Form.Label className='inputField-label-exercise'>{label}</Form.Label>
      <Form.Control
        name={name}
        className='inputField-input-exercise'
        type={type}
        value={fieldValue}
        placeholder={placeholder}
        onChange={handleChange}
        disabled={disabled}
      />
    </div>
  );
};
export default InputFieldExercise;
