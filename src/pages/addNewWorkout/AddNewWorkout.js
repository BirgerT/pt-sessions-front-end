import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router';

// Redux
import { useDispatch, useSelector, connect } from 'react-redux';
import { workoutPostAction } from '../../store/actions/workoutActions';
import { exercisesGetAction } from '../../store/actions/exerciseActions';

// Components
import BaseButton from '../../components/baseButton/BaseButton.js';
import InputField from '../../components/inputField/InputField.js';
import InputTextArea from '../../components/inputTextArea/InputTextArea.js';
import SubmitToast from '../../components/submitToast';
import ExerciseForm from './ExerciseForm';
import withAuth from '../../components/HOCs/auth/WithAuth';

// External components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { GrAdd } from 'react-icons/gr';

// Style
import './AddNewWorkout.scss';

// Data objects
const emptySet = () =>
  Object.freeze({
    numberOfReps: 0,
    weightKG: 0,
    restPeriodSeconds: 0,
    tempo: '0-0-0-0',
  });

const emptyExercise = {
  sets: [emptySet(), emptySet()],
};

// The main component
const AddNewWorkout = ({ history, auth }) => {
  const dispatch = useDispatch();

  // Local state
  const [workout, setWorkout] = useState({ day: 1, week: 1 });
  const [workoutExercises, setWorkoutExercises] = useState([]);

  const { exercises } = useSelector((state) => state.exerciseReducer);
  const [exerciseOptions, setExerciseOptions] = useState([]);

  const [toast, setToast] = useState({ show: false, message: '' });

  // Fetch exercises when entering the page.
  useEffect(() => {
    if (dispatch && auth)
      dispatch(exercisesGetAction(auth.user, handleFailedGet));
  }, [dispatch, auth]);

  // Map exercises to options for the selector.
  useEffect(() => {
    setExerciseOptions(
      exercises.map((e) => {
        return {
          label: e.name,
          value: e.id,
        };
      })
    );
  }, [exercises]);

  /** Event Handlers */

  // Update workout state when form changes
  const handleWorkoutChange = (e) => {
    setWorkout({
      ...workout,
      [e.target.name]: e.target.value.trim(),
    });
  };

  // Update exercise state when form changes
  const handleExerciseChange = (name, id, sets, index) => {
    let newExercises = [...workoutExercises];
    newExercises[index] = { name, id, sets };
    setWorkoutExercises(newExercises);
  };

  // Adds new exercise
  const handleAddExerciseClick = (e) => {
    e.preventDefault();
    setWorkoutExercises([...workoutExercises, emptyExercise]);
  };

  // Removes exercise
  const handleRemoveExerciseClick = (e, index) => {
    e.preventDefault();
    const updatedExercises = workoutExercises.filter((e, i) => i !== index);
    setWorkoutExercises([...updatedExercises]);
  };

  // Save workout
  const handleSaveClicked = (e) => {
    if (workout) {
      e.preventDefault();
      workout.exercises = workoutExercises;
      dispatch(
        workoutPostAction(workout, auth.user, handleSuccess, handleFailure)
      );
    } else {
      setToast({ show: true, message: 'Missing something' });
    }
  };

  // Handle successful post
  const handleSuccess = () => {
    history.push('/workouts');
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to add workout' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <>
      <h1 className='page-heading'> Add new workout </h1>
      <Container>
        <Form aria-label='Workout information'>
          <Row>
            <Col>
              <InputField
                name='Name'
                type='text'
                label={'Name'}
                placeholder={'Name of the workout'}
                onChange={handleWorkoutChange}
                eventOnChange // Makes onChange return the entire event
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <InputTextArea
                name='Description'
                type='text'
                label={'Description'}
                placeholder={'Description'}
                onChange={handleWorkoutChange}
                rows='2'
                eventOnChange
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <h3>
                <b>Exercises</b>
              </h3>
            </Col>
          </Row>

          {workoutExercises.map((exercise, index) => {
            return (
              <ExerciseForm
                key={index}
                exercise={exercise}
                exerciseIndex={index}
                options={exerciseOptions}
                onChange={handleExerciseChange}
                onRemove={handleRemoveExerciseClick}
              />
            );
          })}

          <Row>
            <Col>
              <button
                className='add-exercise-btn'
                onClick={handleAddExerciseClick}
              >
                <GrAdd /> Add exercise
              </button>
            </Col>
          </Row>

          <center>
            <Container className='buttonContainer'>
              <Row>
                <Col>
                  <BaseButton variant='cancel' text='Cancel' link='/workouts'>
                    Cancel
                  </BaseButton>
                </Col>

                <Col>
                  <BaseButton
                    variant='success'
                    text='Save'
                    onClick={handleSaveClicked}
                  >
                    Save
                  </BaseButton>
                </Col>
              </Row>
            </Container>
          </center>
        </Form>
      </Container>

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withRouter(
  withAuth(connect(mapStateToProps, null)(AddNewWorkout))
);
