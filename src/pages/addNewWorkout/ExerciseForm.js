import React, { useEffect, useState } from 'react';

// Components
import ExerciseSetForm from './ExerciseSetForm';
import CustomSelect from '../../components/customSelect';
import FloatRight from '../../components/floatRight';
import withAuth from '../../components/HOCs/auth/WithAuth';

// External components
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import Accordion from 'react-bootstrap/Accordion';
import { GrAdd } from 'react-icons/gr';
import { GrTrash } from 'react-icons/gr';

// Style
import './AddNewWorkout.scss';

const ExerciseForm = ({
  exercise,
  exerciseIndex,
  options,
  disabled,
  onChange,
  onRemove,
}) => {
  const emptySet = {
    numberOfReps: 0,
    weightKG: 0,
    restPeriodSeconds: 0,
    tempo: '0-0-0-0',
  };
  const [selectedOption, setSelectedOption] = useState();
  const [sets, setSets] = useState(exercise.sets);

  // Set selected option to match the exercise.
  useEffect(() => {
    if (exercise && options) {
      options.forEach((o) => {
        if (o.value === exercise.id) setSelectedOption(o);
      });
    }
  }, [options, exercise]);

  // Whenever local state changes, pass it on to the parent.
  useEffect(() => {
    if (selectedOption) {
      onChange(selectedOption.label, selectedOption.value, sets, exerciseIndex);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedOption, sets, exerciseIndex]);

  // Adds new empty set
  const handleAddSetClick = (e) => {
    e.preventDefault();
    const newSetList = [...sets, emptySet];
    setSets(newSetList);
  };

  // Removes set
  const handleRemoveSetClick = (index) => {
    const newSetList = sets.filter((s, i) => i !== index);
    setSets(newSetList);
  };

  // Handle change of set details
  const handleSetChange = (set, index) => {
    let newSetList = [...sets];
    newSetList[index] = set;
    setSets(newSetList);
  };

  // Pick a different exercise
  const handleExerciseChange = (e) => {
    setSelectedOption(e);
  };

  return (
    <>
      <Container>
        <Accordion>
          <Accordion.Toggle as={Card.Header} eventKey='0'>
            <h5>
              {exerciseIndex + 1}: {selectedOption?.label}
            </h5>
          </Accordion.Toggle>

          <Accordion.Collapse eventKey='0'>
            <>
              <Row>
                <Col>
                  <CustomSelect
                    options={options}
                    selectedOption={selectedOption}
                    onChange={handleExerciseChange}
                    disabled={disabled}
                    id='small'
                  />
                </Col>
              </Row>

              {sets.map((set, index) => {
                return (
                  <ExerciseSetForm
                    key={index}
                    index={index}
                    set={set}
                    onChange={handleSetChange}
                    removeSet={handleRemoveSetClick}
                    disabled={disabled}
                  />
                );
              })}

              {!disabled && (
                <Row>
                  <Col>
                    {' '}
                    <button className='add-set-btn' onClick={handleAddSetClick}>
                      {' '}
                      <GrAdd /> Add set{' '}
                    </button>
                  </Col>

                  <Col>
                    {' '}
                    <FloatRight>
                      <button
                        onClick={(e) => onRemove(e, exerciseIndex)}
                        className='add-set-btn'
                      >
                        <GrTrash /> Remove exercise
                      </button>
                    </FloatRight>{' '}
                  </Col>
                </Row>
              )}
            </>
          </Accordion.Collapse>
        </Accordion>
      </Container>
    </>
  );
};

export default withAuth(ExerciseForm);
