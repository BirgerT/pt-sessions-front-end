import React from 'react';
import './AddNewWorkout.scss';
import clear from '../../assets/img/clear.png';

// Components
import InputFieldExercise from './InputFieldExercise';
import withAuth from '../../components/HOCs/auth/WithAuth';

// Bootstrap components
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const ExerciseSetForm = ({ set, index, onChange, removeSet, disabled }) => {
  const handleChange = (e) => {
    const value =
      e.target.type === 'number' ? Number(e.target.value) : e.target.value;

    const newSet = { ...set, [e.target.name]: value };
    onChange(newSet, index);
  };

  return (
    <>
      <Row>
        <Col>
          <div className='center'>
            <p className='addNewExercise-set-number'> {index + 1} </p>
          </div>
        </Col>
        <Col>
          <InputFieldExercise
            name='numberOfReps'
            value={set.numberOfReps}
            type='number'
            label='Reps'
            onChange={handleChange}
            disabled={disabled}
          />
        </Col>
        <Col>
          <InputFieldExercise
            name='weightKG'
            value={set.weightKG}
            type='number'
            label='Weight'
            onChange={handleChange}
            disabled={disabled}
          />
        </Col>
        <Col>
          <InputFieldExercise
            name='restPeriodSeconds'
            value={set.restPeriodSeconds}
            type='number'
            label='Rest'
            onChange={handleChange}
            disabled={disabled}
          />
        </Col>
        <Col>
          <InputFieldExercise
            name='tempo'
            value={set.tempo}
            type='text'
            label='Tempo'
            onChange={handleChange}
            disabled={disabled}
          />
        </Col>
        <Col>
          <div className='center'>
            {!disabled && index > 0 && (
              <img
                src={clear}
                alt=''
                className='icon-clear'
                onClick={() => removeSet(index)}
                value={index}
              />
            )}{' '}
            {/* Show icon if nr of sets>1 */}
          </div>
        </Col>
      </Row>
      <hr />
    </>
  );
};

export default withAuth(ExerciseSetForm);
