import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router';

// Redux
import { useDispatch, useSelector, connect } from 'react-redux';
import { programsPostAction } from '../../store/actions/programActions';
import { clientsGetAction } from '../../store/actions/clientActions';

// Components
import BaseButton from '../../components/baseButton';
import InputField from '../../components/inputField';
import InputTextArea from '../../components/inputTextArea';
import CustomSelect from '../../components/customSelect';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

// External components
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const AddNewProgram = ({ history, auth }) => {
  const dispatch = useDispatch();

  const { clients } = useSelector((state) => state.clientReducer);
  const [selectedClient, setSelectedClient] = useState();
  const clientOptions = clients.map((c) => ({
    id: c.id,
    value: c.firstName + ' ' + c.lastName,
    label: c.firstName + ' ' + c.lastName,
  }));

  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [start, setStart] = useState();
  const [end, setEnd] = useState();

  const [toast, setToast] = useState({ show: false, message: '' });

  // Fetch clients
  useEffect(() => {
    if (dispatch && auth)
      dispatch(clientsGetAction(auth.user, handleFailedGet));
  }, [dispatch, auth]);

  // Save program
  const handleSubmit = (e) => {
    e.preventDefault();
    if (name && description && start && end && selectedClient) {
      const clientId = selectedClient.id;
      const data = {
        name,
        description,
        clientId,
        start,
        end,
      };
      dispatch(
        programsPostAction(data, auth.user, handleSuccess, handleFailure)
      );
    } else {
      setToast({ show: true, message: 'Missing something' });
    }
  };

  // Handle successful post
  const handleSuccess = () => {
    history.push('/programs');
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to add program' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <>
      <h1 className='page-heading'> Add new program </h1>
      <Form aria-label='Program information' onSubmit={handleSubmit}>
        <Container>
          <Row>
            <Col>
              <InputField
                type='text'
                label={'Program name'}
                placeholder={'Program name'}
                onChange={setName}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputTextArea
                type='text'
                label={'Description'}
                placeholder={'Description'}
                onChange={setDescription}
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <CustomSelect
                options={clientOptions}
                label='Client'
                setSelectedOption={setSelectedClient}
                selectedOption={selectedClient}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                type='date'
                label={'Start date'}
                placeholder={'Start date'}
                onChange={setStart}
              />
            </Col>
            <Col>
              <InputField
                type='date'
                label={'End date'}
                placeholder={'End date'}
                onChange={setEnd}
              />
            </Col>
          </Row>
        </Container>

        <Container className='buttonContainer'>
          <Row>
            <Col>
              <BaseButton variant='cancel' text='Cancel' link='/programs'>
                Cancel
              </BaseButton>
            </Col>
            <Col>
              <BaseButton variant='success' text='Save' type='submit'>
                Save
              </BaseButton>
            </Col>
          </Row>
        </Container>
      </Form>

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withRouter(
  withAuth(connect(mapStateToProps, null)(AddNewProgram))
);
