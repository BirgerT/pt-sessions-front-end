import React, { useState } from 'react';
import { withRouter } from 'react-router';

// Redux
import { useDispatch, connect } from 'react-redux';
import { exercisePostAction } from '../../store/actions/exerciseActions';

// Components
import InputField from '../../components/inputField';
import InputTextArea from '../../components/inputTextArea';
import BaseButton from '../../components/baseButton';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

// External Components
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const AddNewExercise = ({ history, auth }) => {
  const dispatch = useDispatch();

  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [category, setCategory] = useState();
  const [type, setType] = useState();

  const [toast, setToast] = useState({ show: false, message: '' });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (name && description && category && type) {
      const data = {
        name,
        description,
        categories: [
          {
            name: category,
          },
        ],
        types: [
          {
            name: type,
          },
        ],
      };
      dispatch(
        exercisePostAction(data, auth.user, handleSuccess, handleFailure)
      );
    } else {
      setToast({ show: true, message: 'Missing something' });
    }
  };

  // Handle successful post
  const handleSuccess = () => {
    history.push('/exercises');
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to add exercise' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <>
      <h1 className='page-heading'>Add new exercise</h1>
      <Form aria-label='Exercise information' onSubmit={handleSubmit}>
        <Container>
          <Row>
            <Col>
              <InputField
                type='text'
                label={'Exercise name'}
                placeholder={'Exercise name'}
                onChange={setName}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputTextArea
                type='text'
                label={'Description'}
                placeholder={'Description'}
                onChange={setDescription}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                type='text'
                label={'Target area'}
                placeholder={'Shoulders, toes, ...'}
                onChange={setCategory}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                type='text'
                label={'Type'}
                placeholder={'Flexibility, strength, ...'}
                onChange={setType}
              />
            </Col>
          </Row>
        </Container>

        <Container className='buttonContainer'>
          <Row>
            <Col>
              <BaseButton variant='cancel' text='Cancel' link='/exercises'>
                Cancel
              </BaseButton>
            </Col>
            <Col>
              <BaseButton variant='success' text='Save' type='submit'>
                Save
              </BaseButton>
            </Col>
          </Row>
        </Container>
      </Form>

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withRouter(
  withAuth(connect(mapStateToProps, null)(AddNewExercise))
);
