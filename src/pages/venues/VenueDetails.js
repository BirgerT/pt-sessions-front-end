import React, { useEffect, useState } from 'react';

//Components
import InputField from '../../components/inputField';
import InputTextArea from '../../components/inputTextArea';
import Form from 'react-bootstrap/Form';
import CustomSelect from '../../components/customSelect';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const typeOptions = [
  {
    id: 1,
    name: 'Home',
    value: 'Home',
    label: 'Home',
  },
  {
    id: 2,
    name: 'Gym',
    value: 'Gym',
    label: 'Gym',
  },
  {
    id: 3,
    name: 'Public',
    value: 'Public',
    label: 'Public',
  },
];

const VenueDetails = ({ data, disabled, onChange }) => {
  const [selectedType, setSelectedType] = useState({});

  useEffect(() => {
    setSelectedType(typeOptions.find((option) => option.id === data.type.id));
  }, [data]);

  useEffect(() => {
    onChange({
      name: 'type',
      value: { id: selectedType.id, name: selectedType.name },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedType]);

  return (
    <div>
      <Form>
        <Container>
          <Row>
            <Col>
              <InputField
                name='name'
                disabled={disabled}
                value={data.name}
                type='text'
                label={'Name'}
                placeholder={'Venue name'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <InputTextArea
                name='description'
                disabled={disabled}
                value={data.description}
                type='text'
                label={'Description'}
                placeholder={'Description'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <InputField
                name='address'
                disabled={disabled}
                type='text'
                value={data.address}
                label={'Address'}
                placeholder={'Address'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
            <Col>
              <div className='dropdown'>
                <CustomSelect
                  name='type'
                  setSelectedOption={setSelectedType}
                  selectedOption={selectedType}
                  disabled={disabled}
                  options={typeOptions}
                  label={'Venue Type'}
                />
              </div>
            </Col>
          </Row>
        </Container>
      </Form>
    </div>
  );
};

export default VenueDetails;
