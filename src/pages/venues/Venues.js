import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector, connect } from 'react-redux';
import {
  venuesGetAction,
  venuesPutAction,
} from '../../store/actions/venueActions';

//Components
import VenueDetails from './VenueDetails';
import BaseButton from '../../components/baseButton';
import FloatRight from '../../components/floatRight';
import InfoTable from '../../components/infoTable';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

//Table columns
const columns = [
  {
    name: 'Name',
    selector: 'name',
    sortable: true,
  },
  {
    name: 'Description',
    selector: 'description',
    sortable: true,
  },
  {
    name: 'Address',
    selector: 'address',
    sortable: true,
  },
  {
    name: 'Type',
    selector: 'type.name',
    sortable: true,
  },
];

const Venues = ({ auth }) => {
  const { venues } = useSelector((state) => state.venueReducer);
  const dispatch = useDispatch();

  const [toast, setToast] = useState({ show: false, message: '' });

  // GET venues when component is loaded
  useEffect(() => {
    dispatch(venuesGetAction(auth.user, handleFailedGet));
  }, [dispatch, auth]);

  // Send PUT request whenever a venue is updated.
  const handleEdit = (venue) => {
    dispatch(venuesPutAction(venue, auth.user, handleSuccess, handleFailure));
  };

  // Handle successful post
  const handleSuccess = () => {
    setToast({ show: true, message: 'Venue updated' });
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to update venue' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <div className='venues-page'>
      <h1 className='page-heading'>Venues</h1>
      <FloatRight>
        <BaseButton type='add' text='Add venue' link='/add-new-venue' />
      </FloatRight>
      <InfoTable
        columns={columns}
        data={venues}
        expandedDetails={<VenueDetails />}
        onEdit={handleEdit}
        withFilter
      />

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(Venues));
