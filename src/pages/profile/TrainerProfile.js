import React, { useState, useEffect } from 'react'
import Form from 'react-bootstrap/Form'
import BaseButton from '../../components/baseButton/BaseButton.js'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import axios from 'axios';
import './TrainerProfile.scss';
import config from '../../config.json';
import * as yup from 'yup';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import profilePicturePlaceholder from '../../assets/img/profilePicturePlaceholder.jpg';
import withAuth from '../../components/HOCs/auth/WithAuth';
import loadingSpinner from '../../assets/img/loadingSpinner.gif';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';


const TrainerSchema = yup.object().shape({
  firstName: yup.string().required('Required'),
  lastName: yup.string().required('Required'),
  email: yup.string().email('Invalid email').required('Required'),
  dob: yup.string().required('Required')
});


const TrainerProfile = props => {
  const [messageVisible, setMessageVisible] = useState(false);
  const [messageText, setMessageText] = useState('Hello there sir!');
  const [isLoading, setIsLoading] = useState(true);
  const [initialValues, setInitialValues] = useState({});
  const [profilePictureInputLabel, setProfilePictureInputLabel] = useState('Select a picture');
  const [profilePictureSource, setProfilePictureSource] = useState(profilePicturePlaceholder);

  useEffect(() => {
    const trainerId = props.auth.trainerId;

    async function getInitialData() {
      try {
        const resp = await axios.get(`${config.PT_SESS_API_BASE_URL}trainers/${trainerId}`, {
          headers: {
            'Authorization': `Bearer ${props.auth.user}`
          }
          
        });
        
        // Remove the time part of the datetime
        resp.data.dob = resp.data.dob.split('T')[0];

        // If a picture is present(ie. non-empty string), try to use it.
        if (resp.data.profilePicture) {
          try {
            await axios.get(`${config.PT_SESS_API_BASE_URL}trainers/${trainerId}/picture`, {
              headers: {
              'Authorization': `Bearer ${props.auth.user}`
              }
            });
            setProfilePictureSource(`${config.PT_SESS_API_BASE_URL}trainers/${trainerId}/picture`);
          }
          catch(error) {
            if (error.response.status === 404) {
              // Picture isn't there, anyway. Use the placeholder.
              setProfilePictureSource(profilePicturePlaceholder);
            }
          }
        }
        setInitialValues(resp.data);
        setIsLoading(false);
      }
      catch(error) {
        if (error.response.status === 404) {
          props.history.push('/notfound')
        }
        else {
          setMessageText(error.message);
          setMessageVisible(true);
        }
      }
    }

    getInitialData();
  }, [props, setInitialValues, setIsLoading])

  async function updateProfilePicture(e) {
    // Event doesn't contain info about whether 'Open' or 'Cancel' was pressed in dialog.
    // So we check manually
    if (e.target.files.length === 0) {
      return;
    }
    else if (e.target.files[0].name === undefined) {
      return;
    }

    setProfilePictureSource(loadingSpinner);
    const trainerId = props.auth.trainerId;

    const file = e.target.files[0];
    setProfilePictureInputLabel(file.name)

    const formData = new FormData();
    formData.append("file", file);
    try {
      await axios.post(`${config.PT_SESS_API_BASE_URL}trainers/${trainerId}/picture`, formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': `Bearer ${props.auth.user}`
          }
      });

      setProfilePictureSource(`${config.PT_SESS_API_BASE_URL}trainers/${trainerId}/picture`);
    }
    catch(error) {
      setMessageText(`Unable to upload your image.\nError: ${error.message}.`);
      setMessageVisible(true);
    }    
  }

  async function register(values, { setSubmitting }) {
    setSubmitting(true);
    try {
      const resp = await axios.put(`${config.PT_SESS_API_BASE_URL}trainers/${props.auth.trainerId}`, values, {
        headers: {
          Authorization: `Bearer ${props.auth.user}`
        }
      });

      // All g, let's get out of here. (201 = Created)
      if (resp.status === 201) {
        window.location.href = config.PT_SESS_FRONT_BASE_URL + 'login';
        return;
      }
    }
    catch(error) {
      setMessageText("Unable to register.\nError: ", error.message);
      setMessageVisible(true);
    }
    finally {
      setSubmitting(false);
    }
  };

  const profileContent = (
    <div className="trainerProfile">
        <Modal show={messageVisible} onHide={ () => setMessageVisible(false) }>
          <Modal.Header closeButton>
            <Modal.Title>A terrible error</Modal.Title>
          </Modal.Header>
          <Modal.Body>{ messageText }</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={ () => setMessageVisible(false) }>
              Close
            </Button>
          </Modal.Footer>
        </Modal>

        <h1 className="page-heading">Profile</h1>
        <Formik
          initialValues={initialValues}
          validationSchema={TrainerSchema}
          onSubmit={register}
        >
          {({ errors, touched, values, handleChange, handleSubmit, handleBlur, isSubmitting, isValid }) => (
            <Form aria-label="Trainer contact information" onSubmit={handleSubmit}>
            <Container>
              <Row>
                <Col md='6' className='profilePictureContainer'>
                  <img src={profilePictureSource} alt='A visual representation of you' />
                  <Form.File 
                    id='profilePictureControl'
                    custom
                  >
                    <Form.File.Label>{ profilePictureInputLabel }</Form.File.Label>
                    <Form.File.Input onChange={updateProfilePicture} />
                  </Form.File>
                </Col>
                <Col md='6' className='nameContainer'>
                <Form.Group>
                  <Form.Label>First name</Form.Label>
                  <Form.Control
                    type='text'
                    name='firstName'
                    value={values.firstName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={!!errors.firstName}
                    className={touched.firstName && errors.firstName ? 'error' : null}
                  />
                  <Form.Control.Feedback type='invalid'>
                    {errors.firstName}
                  </Form.Control.Feedback>
                </Form.Group>
                  <Form.Group >
                    <Form.Label>Middle name</Form.Label>
                    <Form.Control
                      type='text'
                      name='middleName'
                      value={values.middleName}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={touched.middleName && errors.middleName ? 'error' : null}
                    />
                  </Form.Group>
                  <Form.Group >
                    <Form.Label>Last name</Form.Label>
                    <Form.Control
                      type='text'
                      name='lastName'
                      value={values.lastName}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={!!errors.lastName}
                      className={touched.lastName && errors.lastName ? 'error' : null}
                    />
                    <Form.Control.Feedback type='invalid'>
                    {errors.lastName}
                  </Form.Control.Feedback>
                  </Form.Group>
                  </Col>
              </Row>
              <Row>
                <Form.Group as={Col} >
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type='email'
                    name='email'
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur} 
                    isInvalid={!!errors.email}
                    className={touched.email && errors.email ? 'error' : null}
                  />
                  <Form.Control.Feedback type='invalid'>
                    {errors.email}
                  </Form.Control.Feedback>
                </Form.Group>
              </Row>
              <Row>
                <Form.Group as={Col} >
                  <Form.Label>Address</Form.Label>
                  <Form.Control
                    type='text'
                    name='address'
                    value={values.address}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={touched.address && errors.address ? 'error' : null}
                  />
                </Form.Group>
              </Row>
              <Row>
                <Form.Group as={Col} >
                  <Form.Label>Date of birth</Form.Label>
                  <Form.Control 
                    type='date'
                    name='date'
                    value={values.dob}
                    onChange={handleChange}
                    onBlur={handleBlur} 
                    isInvalid={!!errors.dob}
                    className={touched.date && errors.date ? 'error' : null}
                  />
                  <Form.Control.Feedback type='invalid'>
                    {errors.dob}
                  </Form.Control.Feedback>
                </Form.Group>
              </Row>
              <Row>
                <Col><BaseButton link='/logout' variant='danger' text='Log out' /></Col>
                <Col><BaseButton variant='success' type='submit' text='Update' disabled={isSubmitting || !isValid} /></Col>
              </Row>
            </Container>
          </Form>
          )}
        </Formik>
    </div>
  );

  const loadingContent = (
    <div className='pageLoader'>Loading profile...</div>
  );

  if (isLoading) {
    return loadingContent;
  }
  return profileContent;
}

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

export default withAuth(connect(mapStateToProps, null)(TrainerProfile));