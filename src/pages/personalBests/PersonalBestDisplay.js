import React from 'react';
import { Line } from 'react-chartjs-2';
import InfoTable from '../../components/infoTable';

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
};

const columns = [
  {
    name: 'Value',
    selector: 'value',
    sortable: true,
  },
  {
    name: 'Date',
    selector: 'achieved',
    sortable: true,
  },
];

const PersonalBestDisplay = ({ pbs, graphActive }) => {
  const dates = pbs.map((pb) => pb.achieved.split('T')[0]);
  const values = pbs.map((pb) => pb.value);

  const formattedPbs = pbs.map((pb) => ({
    achieved: pb.achieved.split('T')[0],
    value: `${pb.value} ${pb.unit}`,
  }));

  const lineData = {
    labels: dates,
    datasets: [
      {
        label: 'Personal Bests',
        data: values,
        fill: false,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgba(255, 99, 132, 0.2)',
      },
    ],
  };

  if (graphActive) {
    return (
      <>
        <Line data={lineData} options={options} />
      </>
    );
  } else {
    return (
      <InfoTable disableExpandableRows columns={columns} data={formattedPbs} />
    );
  }
};

export default PersonalBestDisplay;
