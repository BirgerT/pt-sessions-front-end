import React, { useState, useEffect } from 'react';
import {
  Button,
  ButtonGroup,
  Col,
  Container,
  Form,
  Row,
} from 'react-bootstrap';
import { useDispatch, useSelector, connect } from 'react-redux';
import { useLocation } from 'react-router';
import CustomSelect from '../../components/customSelect';
import FloatRight from '../../components/floatRight';
import withAuth from '../../components/HOCs/auth/WithAuth';
import InputField from '../../components/inputField';
import { pbPostAction, pbsGetAction } from '../../store/actions/pbActions';
import { exercisesGetAction } from '../../store/actions/exerciseActions';
import PersonalBestDisplay from './PersonalBestDisplay';
import './personalBests.scss';
import SubmitToast from '../../components/submitToast';

const PersonalBests = ({ auth }) => {
  const { clientState } = useLocation();

  const { exercises } = useSelector((state) => state.exerciseReducer);
  const { pbs } = useSelector((state) => state.pbReducer);
  const dispatch = useDispatch();

  const [newPbs, setNewPbs] = useState(pbs);

  const [selectedCurrentOption, setSelectedCurrentOption] = useState('');
  const [currentOptions, setCurrentOptions] = useState([]);
  const [selectedNewOption, setSelectedNewOption] = useState([]);
  const [newOptions, setNewOptions] = useState([]);

  const [graphActive, setGraphActive] = useState(false);
  const [toast, setToast] = useState({ show: false, message: '' });

  const [newPersonalBest, setNewPersonalBest] = useState('');
  const [newDate, setNewDate] = useState('');
  const [newUnit, setNewUnit] = useState('KG');

  //Get pbs and exercises when loading the component
  useEffect(() => {
    if (dispatch && auth && clientState) {
      dispatch(exercisesGetAction(auth.user, handleFailedGet));
      dispatch(pbsGetAction(clientState.client.id, auth.user, handleFailedGet));
    }
  }, [dispatch, auth, clientState]);

  useEffect(() => {
    setNewPbs(pbs);
  }, [pbs]);

  //Fetches the clients exercises where they have a registered pb
  useEffect(() => {
    //Create a set so the currentOptions for the selector will not get duplicate entries
    let uniqueExercises = [...new Set(newPbs.map((pb) => pb.exercise.name))];
    setCurrentOptions(
      uniqueExercises.map((exercise) => ({
        value: exercise,
        label: exercise,
      }))
    );
  }, [newPbs]);

  useEffect(() => {
    if (!selectedCurrentOption) {
      setSelectedCurrentOption(currentOptions[0]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentOptions]);

  //Fetches all exercises for adding new pbs to client
  useEffect(() => {
    let uniqueExercises = [...new Set(exercises.map((ex) => ex.name))];
    setNewOptions(
      uniqueExercises.map((exercise) => ({
        value: exercise,
        label: exercise,
      }))
    );
  }, [exercises]);

  useEffect(() => {
    setSelectedNewOption(newOptions[0]);
  }, [newOptions]);

  const onGraphClick = () => {
    setGraphActive(true);
  };

  const onTableClick = () => {
    setGraphActive(false);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const newExercise = selectedNewOption.value;
    const data = {
      exerciseName: newExercise,
      value: newPersonalBest,
      achieved: newDate,
      unit: newUnit,
    };
    const pb = {
      exercise: {
        name: newExercise,
      },
      value: newPersonalBest,
      achieved: newDate,
      unit: newUnit,
    };
    dispatch(
      pbPostAction(
        data,
        clientState.client.id,
        auth.user,
        handleSuccess,
        handleFailure
      )
    );
    setNewPbs([...pbs, pb]);
  };

  // Handle successful post
  const handleSuccess = () => {
    setToast({ show: true, message: 'Successfully added PB' });
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to add PB' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  if (pbs.length < 1) {
    return <p>This client has set no personal bests</p>;
  }
  return (
    <div>
      <h1 className='page-header'>Personal Best History</h1>
      {selectedCurrentOption ? (
        <div>
          <CustomSelect
            setSelectedOption={setSelectedCurrentOption}
            selectedOption={selectedCurrentOption}
            options={currentOptions}
            label={'Exercise'}
          />
          <div className='button-container'>
            <ButtonGroup aria-label='Basic example'>
              <Button
                active={graphActive}
                variant='outline-success'
                onClick={onGraphClick}
              >
                Graph
              </Button>
              <Button
                active={!graphActive}
                variant='outline-success'
                onClick={onTableClick}
              >
                Table
              </Button>
            </ButtonGroup>
          </div>

          <PersonalBestDisplay
            graphActive={graphActive}
            pbs={newPbs.filter(
              (pb) => pb.exercise.name === selectedCurrentOption.value
            )}
          />
          <Form onSubmit={handleSubmit}>
            <Container>
              <h3>Add new Personal Best</h3>
              <Row>
                <Col>
                  <div className='dropdown'>
                    <CustomSelect
                      setSelectedOption={setSelectedNewOption}
                      selectedOption={selectedNewOption}
                      options={newOptions}
                      label={'Exercise'}
                    />
                  </div>
                </Col>
                <Col>
                  <InputField
                    type='text'
                    onChange={setNewPersonalBest}
                    label={'Personal Best'}
                  />
                </Col>
                <Col className='unit-field' xs={1}>
                  <InputField
                    disabled
                    type='text'
                    onChange={setNewUnit}
                    value={newUnit}
                  />
                </Col>

                <Col>
                  <InputField
                    type='date'
                    onChange={setNewDate}
                    label={'Date'}
                    placeholder={'Date'}
                  />
                </Col>
              </Row>
              <Row>
                <Col className='button-col'>
                  <FloatRight>
                    <Button type='submit' variant='success'>
                      Submit
                    </Button>
                  </FloatRight>
                </Col>
              </Row>
            </Container>
          </Form>

          <SubmitToast
            message={toast.message}
            show={toast.show}
            setShow={closeToast}
          />
        </div>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(PersonalBests));
