import React, { useState } from 'react';
import { withRouter } from 'react-router';

// Redux
import { useDispatch, connect } from 'react-redux';
import { clientPostAction } from '../../store/actions/clientActions';

// Components
import InputField from '../../components/inputField';
import BaseButton from '../../components/baseButton';
import FloatRight from '../../components/floatRight';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

// External components
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const AddNewClient = ({ history, auth }) => {
  const dispatch = useDispatch();

  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [address, setAddress] = useState();
  const [email, setEmail] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [dob, setDob] = useState();

  const [toast, setToast] = useState({ show: false, message: '' });

  // Save client
  const handleSubmit = (e) => {
    e.preventDefault();
    if (firstName && lastName && email && phoneNumber && address && dob) {
      const data = {
        firstName,
        lastName,
        email,
        phoneNumber,
        address,
        dob,
      };
      dispatch(clientPostAction(data, auth.user, handleSuccess, handleFailure));
    } else {
      setToast({ show: true, message: 'Missing something' });
    }
  };

  // Handle successful post
  const handleSuccess = () => {
    history.push('/clients');
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to add client' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <>
      <h1 className='page-heading'>Add new client</h1>
      <Form aria-label='Client contact information' onSubmit={handleSubmit}>
        <Container>
          <Row>
            <Col>
              <InputField
                type='text'
                label={'First name'}
                placeholder={'First name'}
                onChange={setFirstName}
              />
            </Col>
            <Col>
              <InputField
                type='text'
                label={'Last name'}
                placeholder={'Last name'}
                onChange={setLastName}
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <InputField
                type='email'
                label={'Email'}
                placeholder={'Email'}
                onChange={setEmail}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                type='text'
                label={'Address'}
                placeholder={'Address'}
                onChange={setAddress}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                type='phone'
                label={'Contact number'}
                placeholder={'Contact number'}
                onChange={setPhoneNumber}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                type='date'
                label={'Date of Birth'}
                placeholder={'Date of Birth'}
                onChange={setDob}
              />
            </Col>
          </Row>

          <Container className='buttonContainer'>
            <Row>
              <Col>
                <BaseButton variant='cancel' text='Cancel' link='/clients'>
                  Cancel
                </BaseButton>
              </Col>
              <Col>
                <FloatRight>
                  <BaseButton variant='success' text='Save' type='submit'>
                    Save
                  </BaseButton>
                </FloatRight>
              </Col>
            </Row>
          </Container>
        </Container>
      </Form>

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withRouter(
  withAuth(connect(mapStateToProps, null)(AddNewClient))
);
