import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector, connect } from 'react-redux';
import {
  workoutPutAction,
  workoutsGetAction,
} from '../../store/actions/workoutActions';
import withAuth from '../../components/HOCs/auth/WithAuth';

import WorkoutDetails from './WorkoutDetails';
import BaseButton from '../../components/baseButton';
import FloatRight from '../../components/floatRight';
import InfoTable from '../../components/infoTable';
import SubmitToast from '../../components/submitToast';

const columns = [
  {
    name: 'Name',
    selector: 'name',
    sortable: true,
  },
  {
    name: 'Description',
    selector: 'description',
    sortable: true,
  },
];

function Workouts({ auth }) {
  const { workouts } = useSelector((state) => state.workoutReducer);
  const dispatch = useDispatch();

  const [toast, setToast] = useState({ show: false, message: '' });

  // Fetch workouts when entering the page.
  useEffect(() => {
    dispatch(workoutsGetAction(auth.user, handleFailedGet));
  }, [dispatch, auth]);

  // Send PUT request whenever a workout is updated.
  const handleEdit = (workout) => {
    dispatch(
      workoutPutAction(workout, auth.user, handleSuccess, handleFailure)
    );
  };

  // Handle successful post
  const handleSuccess = () => {
    setToast({ show: true, message: 'Workout updated' });
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to update workout' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <div className='venues-page'>
      <h1 className='page-heading'>Workouts</h1>
      <FloatRight>
        <BaseButton type='add' text='Add workout' link='/add-new-workout' />
      </FloatRight>
      <InfoTable
        columns={columns}
        data={workouts}
        expandedDetails={<WorkoutDetails />}
        onEdit={handleEdit}
        withFilter
      />

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(Workouts));
