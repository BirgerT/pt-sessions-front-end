import React, { useEffect, useState } from 'react';
import './workouts.scss';

// Redux
import { useDispatch, useSelector, connect } from 'react-redux';
import { workoutExercisesGetAction } from '../../store/actions/workoutActions';
import { exercisesGetAction } from '../../store/actions/exerciseActions';

// Components
import ExerciseForm from '../addNewWorkout/ExerciseForm';
import InputField from '../../components/inputField';
import InputTextArea from '../../components/inputTextArea';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

// External components
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { GrAdd } from 'react-icons/gr';

const WorkoutDetails = ({ data, disabled, onChange, auth }) => {
  const emptyExercise = {
    sets: [
      {
        numberOfReps: 0,
        weightKG: 0,
        restPeriodSeconds: 0,
        tempo: '0-0-0-0',
      },
    ],
  };

  const dispatch = useDispatch();

  const [workoutExercises, setWorkoutExercises] = useState([]);

  const { exercises } = useSelector((state) => state.exerciseReducer);
  const [exerciseOptions, setExerciseOptions] = useState([
    { value: 1, label: 'Workout' },
  ]);

  const [toast, setToast] = useState({ show: false, message: '' });

  // Fetch exercises and exerciseOptions
  useEffect(() => {
    if (dispatch && auth) {
      dispatch(workoutExercisesGetAction(data, auth.user, handleFailedGet));
      dispatch(exercisesGetAction(auth.user, handleFailedGet));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, auth]);

  useEffect(() => {
    if (data.exercises) {
      const inputExercises = data.exercises.map((exercise) => {
        if (exercise.exercise)
          exercise.id = Number(exercise.exercise.url.split('/').pop());
        return exercise;
      });
      setWorkoutExercises(inputExercises);
    }
  }, [data]);

  // Map exercises to options for the selector.
  useEffect(() => {
    setExerciseOptions(
      exercises.map((e) => {
        return {
          label: e.name,
          value: e.id,
        };
      })
    );
  }, [exercises]);

  //Adds new exercise
  const handleAddExerciseClick = (e) => {
    e.preventDefault();
    setWorkoutExercises([...workoutExercises, emptyExercise]);
  };

  //Removes exercise
  const handleRemoveExerciseClick = (e, index) => {
    e.preventDefault();
    const updatedExercises = workoutExercises.filter((e, i) => i !== index);
    setWorkoutExercises([...updatedExercises]);
  };

  // Update exercise state when form changes
  const handleExerciseChange = (name, id, sets, index) => {
    if (name) {
      let newExercises = [...workoutExercises];
      newExercises[index] = { name, id, sets };
      onChange({ name: 'exercises', value: newExercises });
      setWorkoutExercises(newExercises);
    }
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <div>
      <Form>
        <Container>
          <Row>
            <Col>
              <InputField
                name='name'
                disabled={disabled}
                value={data.name}
                type='text'
                label={'Name'}
                placeholder='Name'
                onChange={onChange}
                eventOnChange // makes onChange return the event instead of just the value
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputTextArea
                name='description'
                disabled={disabled}
                value={data.description}
                type='text'
                label={'Description'}
                placeholder='Description'
                onChange={onChange}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <h3>
                <b>Exercises</b>
              </h3>
            </Col>
          </Row>
          {workoutExercises &&
            workoutExercises.map((exercise, index) => {
              return (
                <ExerciseForm
                  key={index}
                  options={exerciseOptions}
                  exercise={exercise}
                  exerciseIndex={index}
                  disabled={disabled}
                  onChange={handleExerciseChange}
                  onRemove={handleRemoveExerciseClick}
                />
              );
            })}
          {!disabled && (
            <Row>
              <Col>
                <button
                  className='add-exercise-btn'
                  onClick={handleAddExerciseClick}
                >
                  <GrAdd /> Add exercise
                </button>
              </Col>
            </Row>
          )}
          {disabled && workoutExercises.length === 0 && (
            <p>This workout contains no exercises yet</p>
          )}
        </Container>
      </Form>

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(WorkoutDetails));
