import React, { useState } from 'react';
import { withRouter } from 'react-router';

// Redux
import { useDispatch, connect } from 'react-redux';
import { venuePostAction } from '../../store/actions/venueActions';

// Components
import BaseButton from '../../components/baseButton';
import InputField from '../../components/inputField';
import InputTextArea from '../../components/inputTextArea';
import CustomSelect from '../../components/customSelect';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

// External components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const AddNewVenue = ({ history, auth }) => {
  const dispatch = useDispatch();

  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [address, setAddress] = useState();

  const [toast, setToast] = useState({ show: false, message: '' });

  const options = [
    { id: 1, name: 'Gym', value: 'Gym', label: 'Gym' },
    { id: 2, name: 'Home', value: 'Home', label: 'Home' },
    { id: 3, name: 'Public', value: 'Public', label: 'Public' },
  ];
  const [selectedOption, setSelectedOption] = useState(options[0]);

  // Save venue
  const handleSubmit = (e) => {
    e.preventDefault();
    if (selectedOption && name && description && address) {
      const typeId = selectedOption.id;
      const data = {
        name,
        description,
        address,
        typeId,
      };
      dispatch(venuePostAction(data, auth.user, handleSuccess, handleFailure));
    } else {
      setToast({ show: true, message: 'Missing something' });
    }
  };

  // Handle successful post
  const handleSuccess = () => {
    history.push('/venues');
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to add venue' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <>
      <h1 className='page-heading'> Add new venue </h1>
      <Form aria-label='Venue information' onSubmit={handleSubmit}>
        <Container>
          <Row>
            <Col>
              <InputField
                type='text'
                label={'Name'}
                placeholder={'Name of the venue'}
                onChange={setName}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputTextArea
                type='text'
                label={'Description'}
                placeholder={'Description'}
                onChange={setDescription}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                type='text'
                label={'Address'}
                placeholder={'Address'}
                onChange={setAddress}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <CustomSelect
                options={options}
                label='Type'
                selectedOption={selectedOption}
                setSelectedOption={setSelectedOption}
              />
            </Col>
          </Row>

          <Container className='buttonContainer'>
            <Row>
              <Col>
                <BaseButton variant='cancel' text='Cancel' link='/venues'>
                  Cancel
                </BaseButton>
              </Col>
              <Col>
                <BaseButton variant='success' text='Save' type='submit'>
                  Save
                </BaseButton>
              </Col>
            </Row>
          </Container>
        </Container>
      </Form>

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withRouter(
  withAuth(connect(mapStateToProps, null)(AddNewVenue))
);
