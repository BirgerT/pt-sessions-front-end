import InfoTable from '../../components/infoTable';
import withAuth from '../../components/HOCs/auth/WithAuth';
import trainerService from '../../services/trainerService';
import React, { useEffect, useState } from 'react';
import config from '../../config.json';
import axios from 'axios';
import { connect } from 'react-redux';

function Trainers(props) {
  const [trainers, setTrainers] = useState([]);

  // Fetch the royal trainers
  useEffect(() => {
    const fetchTrainers = async () => {
      const options = {
        url: config.PT_SESS_API_BASE_URL + 'trainers',
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + props.auth.user
        }
      };
  
      var resp = await axios(options);
      if (resp.statusText !== "OK") {
          // Throw some horrible exception here. TODO
      }
      return resp.data;
    }
    const trainers = fetchTrainers();
    trainers.then(data => setTrainers(data))
  }, []);

  // Whenever the active checkbox is (un)ticked, update db
  async function handleActiveUpdate(e) {
    e.target.disabled = true;
    const row = e.target.parentElement.parentElement;
    // id attributes are of the form [row-KEYFIELD] 
    // where KEYFIELD is [id] by default, if an id field is 
    // present in the [data] supplied to [infotable]
    const idAttr = row.getAttribute('id');

    // Get the actual (trainer) id supplied for the given row. 
    // + 1 to skip the dash.
    const id = idAttr.substr(idAttr.indexOf('-') + 1);

    const active = e.target.checked;

    // Update
    try {
      const cfg = {
        headers: {
          Authorization : `Bearer ${props.auth.user}`
        }
      }
      await axios.post(`${config.PT_SESS_API_BASE_URL}trainers/${id}/active`, {
        id,
        active
      }, cfg);
    }
    catch(error) {
      e.target.checked = !e.target.checked;
    }
    finally {
      e.target.disabled = false;
    }
  }

  const columns = [
    {
      name: 'Name',
      selector: 'name',
      sortable: true,
    },
    {
      name: 'Email',
      selector: 'email',
      sortable: true,
    },
    {
      name: 'Date of birth',
      selector: 'dateOfBirth',
      sortable: true,
    },
    {
      name: 'Active',
      selector: 'active',
      sortable: true,
      cell: (row) => (
        <input
          type="checkbox"
          defaultChecked={row.active}
          onChange={handleActiveUpdate}
        />
      ),
    },
  ];

  const viewTrainers = trainers.map(t => ({
      name: t.firstName + ' ' + (t.middleName ? (t.middleName + ' ') : '') + t.lastName,
      email: t.email,
      dateOfBirth: new Date(t.dob).toLocaleDateString('en-GB'),
      active: t.active,
      id: t.id
  })).reverse();

  return (
    <>
      <h1 className="page-heading">Trainers</h1>
      <InfoTable columns={columns} data={viewTrainers} disableExpandableRows={true} />
    </>
  );
}

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

export default withAuth(connect(mapStateToProps, null)(Trainers), 'admin');