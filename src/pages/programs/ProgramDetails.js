import React, { useEffect, useState } from 'react';

// Redux
import { useDispatch, useSelector, connect } from 'react-redux';
import { clientsGetAction } from '../../store/actions/clientActions';
import { workoutsGetAction } from '../../store/actions/workoutActions';

// Components
import InputField from '../../components/inputField';
import InputTextArea from '../../components/inputTextArea';
import CustomSelect from '../../components/customSelect';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

// External components
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { Button } from 'react-bootstrap';

const ProgramDetails = ({ data, disabled, onChange, auth }) => {
  const dispatch = useDispatch();

  const { clients } = useSelector((state) => state.clientReducer);
  const [clientOptions, setClientOptions] = useState([]);
  const [selectedClient, setSelectedClient] = useState(null);

  const { workouts } = useSelector((state) => state.workoutReducer);
  const [programWorkouts, setProgramWorkouts] = useState([]);
  const [workoutOptions, setWorkoutOptions] = useState([]);
  const [selectedWorkout, setSelectedWorkout] = useState(null);

  const [toast, setToast] = useState({ show: false, message: '' });

  // Fetch clients and workouts from db to populate select
  useEffect(() => {
    if (dispatch && auth) {
      dispatch(clientsGetAction(auth.user, handleFailedGet));
      dispatch(workoutsGetAction(auth.user, handleFailedGet));
    }
  }, [dispatch, auth]);

  // Set selected option to correct client
  useEffect(() => {
    if (clients.length > 0) {
      setClientOptions(
        clients.map((client) => {
          client.name = client.firstName + ' ' + client.lastName;
          client.label = client.name;
          client.value = client.id;
          client.url =
            'http://pt-sessions.azurewebsites.net/api/v1/clients/' + client.id;
          return client;
        })
      );
    }
  }, [clients]);

  // Add fields for the dropdown selector
  useEffect(() => {
    setProgramWorkouts(
      workouts
        .map((workout) => {
          workout.label = workout.name;
          workout.value = workout.id;
          return workout;
        })
        .filter((workout) => workout.programId === data.id)
    );
  }, [workouts, data]);

  // Only allow selection of workouts that have no program
  useEffect(() => {
    if (workouts.length > 0) {
      const workoutOpts = workouts
        .map((workout) => {
          workout.label = workout.name;
          workout.value = workout.id;
          return workout;
        })
        .filter(
          (workout) =>
            workout.programId === 0 && !programWorkouts.includes(workout)
        );
      setWorkoutOptions(workoutOpts);
    }
  }, [workouts, programWorkouts]);

  useEffect(() => {
    if (clientOptions.length > 0) {
      setSelectedClient(
        clientOptions.find((client) => client.url === data.client.url)
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [clientOptions]);

  // Handle selection of different client
  useEffect(() => {
    if (selectedClient) {
      onChange({ name: 'clientId', value: selectedClient.id });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedClient]);

  // Handle update of workout list
  useEffect(() => {
    if (programWorkouts) {
      onChange({
        name: 'workoutIds',
        value: programWorkouts.map((workout) => workout.id),
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [programWorkouts]);

  // Handle add workout
  const handleAddWorkout = (e) => {
    e.preventDefault();
    if (selectedWorkout) {
      setProgramWorkouts([...programWorkouts, selectedWorkout]);
      setSelectedWorkout(null);
    }
  };

  const handleRemoveWorkout = (index) => {
    const updatedWorkouts = programWorkouts.filter((_, i) => i !== index);
    setProgramWorkouts(updatedWorkouts);
  };

  const handleWorkoutChange = (workout) => {
    setSelectedWorkout(workout);
  };

  // Handle failed get
  const handleFailedGet = () => {
    console.log('ProgramDetails: Failed get');
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <>
      <Container>
        <Row>
          <Col>
            <InputField
              name='name'
              disabled={disabled}
              value={data.name}
              type='text'
              label={'Name'}
              placeholder={'Program name'}
              onChange={onChange}
              eventOnChange
            />
          </Col>
          <Col>
            <CustomSelect
              name='clientId'
              setSelectedOption={setSelectedClient}
              selectedOption={selectedClient}
              disabled={disabled}
              options={clientOptions}
              label={'Client'}
            />
          </Col>
        </Row>

        <Row>
          <Col>
            <InputTextArea
              name='description'
              disabled={disabled}
              value={data.description}
              type='text'
              label={'Description'}
              placeholder={'Description'}
              onChange={onChange}
              eventOnChange
            />
          </Col>
        </Row>

        <Row>
          <Col>
            <InputField
              name='start'
              disabled={disabled}
              type='text'
              value={data.start}
              label={'Start Date'}
              placeholder={'Date'}
              onChange={onChange}
              eventOnChange
            />
          </Col>
          <Col>
            <InputField
              name='end'
              disabled={disabled}
              type='text'
              value={data.end}
              label={'End Date'}
              placeholder={'Date'}
              onChange={onChange}
              eventOnChange
            />
          </Col>
        </Row>

        <Row>
          <h2>Workouts:</h2>
        </Row>
        {programWorkouts.map((workout, index) => {
          return (
            <Row key={index}>
              <Col>
                <h3>
                  {!disabled && (
                    <Button onClick={() => handleRemoveWorkout(index)}>
                      Remove
                    </Button>
                  )}
                  {' ' + workout.name}
                </h3>
              </Col>
            </Row>
          );
        })}

        {!disabled && (
          <Row>
            <Col>
              <CustomSelect
                options={workoutOptions}
                selectedOption={selectedWorkout}
                onChange={handleWorkoutChange}
                disabled={disabled}
                id='small'
              />
              <Button onClick={handleAddWorkout}>Add</Button>
            </Col>
          </Row>
        )}
      </Container>

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(ProgramDetails));
