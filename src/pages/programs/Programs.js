import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector, connect } from 'react-redux';
import {
  programsGetAction,
  programsPutAction,
} from '../../store/actions/programActions';

import ProgramDetails from './ProgramDetails';
import BaseButton from '../../components/baseButton';
import FloatRight from '../../components/floatRight';
import InfoTable from '../../components/infoTable';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

const columns = [
  {
    name: 'Name',
    selector: 'name',
    sortable: true,
  },
  {
    name: 'Client',
    selector: 'client.name',
    sortable: true,
  },
  {
    name: 'Start Date',
    selector: 'start',
    sortable: true,
  },
  {
    name: 'End Date',
    selector: 'end',
    sortable: true,
  },
];

const Programs = ({ auth }) => {
  const { programs } = useSelector((state) => state.programReducer);
  const dispatch = useDispatch();

  const [toast, setToast] = useState({ show: false, message: '' });

  useEffect(() => {
    if (dispatch && auth)
      dispatch(programsGetAction(auth.user, handleFailedGet));
  }, [dispatch, auth]);

  const viewPrograms = programs.map((p) => ({
    id: p.id,
    name: p.name,
    description: p.description,
    client: p.client,
    clientId: p.client.id,
    start: (p.start = p.start.split('T')[0]),
    end: (p.end = p.end.split('T')[0]),
  }));

  // Send PUT request whenever a program is updated.
  const handleEdit = (program) => {
    // posted object expected to have clientId, not client name/url
    // hack: extract id from url
    if (program.client && program.client.url) {
      const idIdx = program.client.url.lastIndexOf('/') + 1;
      program.clientId = parseInt(program.client.url.substr(idIdx));
    }
    dispatch(
      programsPutAction(program, auth.user, handleSuccess, handleFailure)
    );

    // hack 2: update page data by re-fetching
    setTimeout(() => {
      dispatch(programsGetAction(auth.user, handleFailedGet));
    }, 500);
  };

  // Handle successful post
  const handleSuccess = () => {
    setToast({ show: true, message: 'Program updated' });
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to update program' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <div className='venues-page'>
      <h1 className='page-heading'>Programs</h1>
      <FloatRight>
        <BaseButton type='add' text='Add program' link='/add-new-program' />
      </FloatRight>
      <InfoTable
        columns={columns}
        data={viewPrograms}
        expandedDetails={<ProgramDetails />}
        onEdit={handleEdit}
        withFilter
      />

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(Programs));
