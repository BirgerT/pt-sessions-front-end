import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  clientsGetAction,
  clientsPutAction,
} from '../../store/actions/clientActions';

import ClientDetails from './ClientDetails';
import BaseButton from '../../components/baseButton';
import FloatRight from '../../components/floatRight';
import InfoTable from '../../components/infoTable';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';
import { connect } from 'react-redux';

const columns = [
  {
    name: 'First Name',
    selector: 'firstName',
    sortable: true,
  },
  {
    name: 'Last Name',
    selector: 'lastName',
    sortable: true,
  },
  {
    name: 'Address',
    selector: 'address',
    sortable: true,
  },
  {
    name: 'Email',
    selector: 'email',
    sortable: true,
  },
  {
    name: 'Phone Number',
    selector: 'phoneNumber',
    sortable: true,
  },
];

const Clients = ({ auth }) => {
  const dispatch = useDispatch();

  const { clients } = useSelector((state) => state.clientReducer);
  const [toast, setToast] = useState({ show: false, message: '' });

  // Fetch client when component is loaded
  useEffect(() => {
    if (dispatch && auth)
      dispatch(clientsGetAction(auth.user, handleFailedGet));
  }, [dispatch, auth]);

  // Send PUT request whenever a client is updated.
  const handleEdit = (client) => {
    dispatch(clientsPutAction(client, auth.user, handleSuccess, handleFailure));
  };

  // Handle successful post
  const handleSuccess = () => {
    setToast({ show: true, message: 'Client updated' });
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to update client' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <div className='clients'>
      <h1 className='page-heading'>Clients</h1>
      <FloatRight>
        <BaseButton type='add' link='/add-new-client' text='Add client' />
      </FloatRight>
      <InfoTable
        columns={columns}
        data={clients}
        expandedDetails={<ClientDetails />}
        onEdit={handleEdit}
        withFilter
      />

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(Clients));
