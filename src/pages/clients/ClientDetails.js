import React, { useState } from 'react';
import InputField from '../../components/inputField/InputField.js';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './clients.scss';

const ClientDetails = ({ data, disabled, onChange }) => {
  data.dob = data.dob.split('T')[0];

  return (
    <div>
      <Container>
        <Form>
          <Row>
            <Col>
              <InputField
                name='firstName'
                disabled={disabled}
                value={data.firstName}
                type='text'
                label={'First Name'}
                placeholder={'First name'}
                onChange={onChange}
                eventOnChange
              />
            </Col>

            <Col>
              <InputField
                name='lastName'
                disabled={disabled}
                value={data.lastName}
                type='text'
                label={'Last Name'}
                placeholder={'Last Name'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                name='email'
                disabled={disabled}
                type='email'
                value={data.email}
                label={'Email'}
                placeholder={'Email'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
            <Col>
              <InputField
                name='phoneNumber'
                disabled={disabled}
                type='text'
                value={data.phoneNumber}
                label={'Phone Number'}
                placeholder={'Phone Number'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                name='address'
                disabled={disabled}
                type='text'
                value={data.address}
                label={'Address'}
                placeholder={'Address'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                name='dob'
                disabled={disabled}
                type={'Date'}
                value={data.dob}
                label={'Date of birth'}
                placeholder={'Date of birth'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <InputField
                name='heightCM'
                disabled={disabled}
                type='text'
                value={data.heightCM}
                label={'Height'}
                placeholder={'Height'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
            <Col>
              <InputField
                name='weightKG'
                disabled={disabled}
                type='text'
                value={data.weightKG}
                label={'Weight'}
                placeholder={'Weight'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>
        </Form>

        <Row>
          <Col>
            <Link
              to={{
                pathname: `/clients/${data.id}/pbs`,
                clientState: { client: data },
              }}
            >
              <Button className='pb-button' variant='success' size='lg' block>
                View Personal Bests
              </Button>
            </Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ClientDetails;
