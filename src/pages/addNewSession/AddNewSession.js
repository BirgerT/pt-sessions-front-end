import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router';

// Redux
import { useDispatch, useSelector, connect } from 'react-redux';
import {
  sessionsGetAction,
  sessionsPostAction,
} from '../../store/actions/sessionActions';
import { clientsGetAction } from '../../store/actions/clientActions';
import { venuesGetAction } from '../../store/actions/venueActions';
import { programsGetAction } from '../../store/actions/programActions';
import { workoutsGetAction } from '../../store/actions/workoutActions';

// Components
import InputField from '../../components/inputField';
import BaseButton from '../../components/baseButton';
import CustomSelect from '../../components/customSelect';
import FloatRight from '../../components/floatRight';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

// External components
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const AddNewSession = ({ history, auth }) => {
  const dispatch = useDispatch();

  const [date, setDate] = useState();
  const [time, setTime] = useState();

  const { clients } = useSelector((state) => state.clientReducer);
  const [clientOptions, setClientOptions] = useState();
  const [selectedClient, setSelectedClient] = useState();

  const { venues } = useSelector((state) => state.venueReducer);
  const [venueOptions, setVenueOptions] = useState();
  const [selectedVenue, setSelectedVenue] = useState();

  const { programs } = useSelector((state) => state.programReducer);
  const [programOptions, setProgramOptions] = useState();
  const [selectedProgram, setSelectedProgram] = useState();

  const { workouts } = useSelector((state) => state.workoutReducer);
  const [workoutOptions, setWorkoutOptions] = useState();
  const [selectedWorkout, setSelectedWorkout] = useState();

  const [toast, setToast] = useState({ show: false, message: '' });

  const { sessions } = useSelector((state) => state.sessionReducer);

  useEffect(() => {
    if (dispatch && auth) {
      dispatch(clientsGetAction(auth.user, handleFailedGet));
      dispatch(venuesGetAction(auth.user, handleFailedGet));
      dispatch(programsGetAction(auth.user, handleFailedGet));
      dispatch(workoutsGetAction(auth.user, handleFailedGet));
      dispatch(sessionsGetAction(auth.user, handleFailedGet));
    }
  }, [dispatch, auth]);

  // Map clients to client options.
  useEffect(() => {
    setClientOptions(
      clients.map((client) => {
        return {
          value: client.id,
          label: client.firstName + ' ' + client.lastName,
        };
      })
    );
  }, [clients]);

  // Map venues to venue options.
  useEffect(() => {
    setVenueOptions(
      venues.map((venue) => {
        return {
          value: venue.id,
          label: venue.name,
        };
      })
    );
  }, [venues]);

  // Map programs to program options.
  useEffect(() => {
    let programOpt = programs.map((program) => {
      return {
        value: program.id,
        label: program.name,
        clientId: Number(program.client.url.split('/').pop()),
        workoutIds: program.workouts.map((workout) =>
          Number(workout.url.split('/').pop())
        ),
      };
    });
    // Only show programs for the selected client.
    if (selectedClient) {
      programOpt = programOpt.filter(
        (program) => program.clientId === selectedClient.value
      );
      setSelectedProgram(programOpt[0] ?? false);
    }
    setProgramOptions(programOpt);
  }, [programs, selectedClient]);

  // Map and filter workouts to workout options.
  useEffect(() => {
    let workoutOpt = workouts.map((workout) => {
      return {
        value: workout.id,
        label: workout.name,
      };
    });
    // Only show workouts for the selected program.
    if (selectedProgram) {
      workoutOpt = workoutOpt.filter((workout) =>
        selectedProgram.workoutIds.includes(workout.value)
      );
      // Deselect workout if it's not in the program.
      if (!workoutOpt.includes(selectedWorkout)) {
        setSelectedWorkout(false);
      }
    }
    // Remove workouts that are already in a session.
    if (sessions) {
      const takenWorkouts = [];
      sessions.forEach((session) => {
        const workoutId = Number(session.workout.url.split('/').pop());
        if (workoutId) takenWorkouts.push(workoutId);
      });
      workoutOpt = workoutOpt.filter(
        (workout) => !takenWorkouts.includes(workout.value)
      );
    }
    setWorkoutOptions(workoutOpt);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [workouts, selectedProgram, sessions]);

  // Post new session to API
  const handleSaveClick = () => {
    if (date && time && selectedProgram && selectedVenue && selectedWorkout) {
      dispatch(
        sessionsPostAction(
          {
            time: date + 'T' + time,
            programId: selectedProgram.value,
            venueId: selectedVenue.value,
            workoutId: selectedWorkout.value,
          },
          auth.user,
          handleSuccess,
          handleFailure
        )
      );
    } else {
      setToast({ show: true, message: 'Missing something' });
    }
  };

  // Handle successful post
  const handleSuccess = () => {
    history.push('/sessions');
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to add session' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <>
      <h1 className='page-heading'> Create new session </h1>
      <Form aria-label='Session information'>
        <Container>
          <Row>
            <Col>
              <InputField
                type='date'
                label={'Date'}
                placeholder={'Date'}
                value={date}
                onChange={setDate}
              />
            </Col>
            <Col>
              <InputField
                type='time'
                label={'Time'}
                placeholder={'Time'}
                value={time}
                onChange={setTime}
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <CustomSelect
                label='Client'
                options={clientOptions}
                selectedOption={selectedClient}
                onChange={setSelectedClient}
              />
            </Col>
            <Col>
              <CustomSelect
                label='Venue'
                options={venueOptions}
                selectedOption={selectedVenue}
                onChange={setSelectedVenue}
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <CustomSelect
                label='Program'
                options={programOptions}
                selectedOption={selectedProgram}
                onChange={setSelectedProgram}
              />
            </Col>
            <Col>
              <CustomSelect
                label='Workout'
                options={workoutOptions}
                selectedOption={selectedWorkout}
                onChange={setSelectedWorkout}
              />
            </Col>
          </Row>
        </Container>
      </Form>

      <Container className='buttonContainer'>
        <Row>
          <Col>
            <BaseButton variant='cancel' text='Cancel' link='/sessions'>
              Cancel
            </BaseButton>
          </Col>
          <Col>
            <FloatRight>
              <BaseButton
                text='Save'
                variant='success'
                onClick={handleSaveClick}
              >
                Save
              </BaseButton>
            </FloatRight>
          </Col>
        </Row>
      </Container>

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withRouter(
  withAuth(connect(mapStateToProps, null)(AddNewSession))
);
