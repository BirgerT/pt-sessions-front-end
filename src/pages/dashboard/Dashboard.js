import React from 'react';
import IconCard from '../../components/iconCard'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import './Dashboard.scss'
import { connect } from 'react-redux';
import withAuth from '../../components/HOCs/auth/WithAuth';
 
const Dashboard = props => {
    const isAdmin = props.auth && props.auth.userRole === 'admin';
    const trainers = isAdmin ? (
      <Row className='dashboard-iconCard-row'>
        <Col xs={6} md={4}><IconCard name='Trainers' link='/trainers'/></Col>
      </Row>) : null;

    return (
    <div className='dashboard-page'>
        <div className='dashboard-greeting'>
            <h1 className='dashboard-greeting-title'>Welcome to the dashboard, {props.auth.userName}!</h1>{/**Skal være navn på bruker */}
            <h3 className='welcome-info'>Click on the shortcuts to navigate.</h3>
        </div>
        <Container>
            <Row className='dashboard-iconCard-row'>
                <Col><IconCard name='Sessions' link='/sessions'/></Col>
                <Col><IconCard name='Exercises' link='/exercises'/></Col>
                <Col><IconCard name='Workouts' link='/workouts'/></Col>
            </Row>
            <Row className='dashboard-iconCard-row'>
                <Col><IconCard name='Programs' link='/programs'/></Col>
                <Col><IconCard name='Venues' link='/venues'/></Col>
                <Col><IconCard name='Clients' link='/clients'/></Col>
            </Row>
            { trainers }
        </Container>
    </div>
    )
};

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default withAuth(connect(mapStateToProps, null)(Dashboard));
