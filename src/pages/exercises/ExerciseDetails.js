import React, { useEffect, useState } from 'react';
import InputField from '../../components/inputField';
import InputTextArea from '../../components/inputTextArea';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const ExerciseDetails = ({ data, disabled, onChange }) => {
  const options = [
    { value: 'endurance', label: 'Endurance' },
    { value: 'balance', label: 'Balance' },
    { value: 'strength', label: 'Strength' },
    { value: 'flexibility', label: 'Flexibility' },
  ];

  const [selectedOption, setSelectedOption] = useState({});

  useEffect(() => {
    setSelectedOption(
      options.find((option) => option.label === data.types[0].name)
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <Form>
        <Container>
          <Row>
            <Col>
              <InputField
                disabled={disabled}
                value={data.name}
                type='text'
                name='name'
                label={'Exercise name'}
                placeholder={'Exercise name'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <InputTextArea
                disabled={disabled}
                value={data.description}
                name='description'
                label={'Description'}
                placeholder={'Description'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>

          <Row>
            <Col>
              <InputField
                disabled={disabled}
                type='text'
                name='categories'
                value={data.categories[0].name}
                label={'Target area'}
                placeholder={'Target area. E.g. shoulders, toes'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
            <Col>
              <InputField
                disabled={disabled}
                type='text'
                name='types'
                value={data.types[0].name}
                label={'Type'}
                placeholder={'Type. E.g. flexibility, strength'}
                onChange={onChange}
                eventOnChange
              />
            </Col>
          </Row>
        </Container>
      </Form>
    </div>
  );
};

export default ExerciseDetails;
