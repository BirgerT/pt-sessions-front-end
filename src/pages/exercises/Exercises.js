import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector, connect } from 'react-redux';
import {
  exercisesGetAction,
  exercisePutAction,
} from '../../store/actions/exerciseActions';

//Components
import ExerciseDetails from './ExerciseDetails';
import BaseButton from '../../components/baseButton/BaseButton';
import FloatRight from '../../components/floatRight';
import InfoTable from '../../components/infoTable';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

function Exercises({ auth }) {
  const columns = [
    {
      name: 'Name',
      selector: 'name',
      sortable: true,
    },
    {
      name: 'Description',
      selector: 'description',
      sortable: true,
    },
  ];

  const dispatch = useDispatch();
  const { exercises } = useSelector((state) => state.exerciseReducer);
  const [toast, setToast] = useState({ show: false, message: '' });

  //Get exercises when loading the component
  useEffect(() => {
    if (dispatch && auth)
      dispatch(exercisesGetAction(auth.user, handleFailedGet));
  }, [dispatch, auth]);

  const handleEdit = (exercise) => {
    const exerciseDto = {
      id: exercise.id,
      name: exercise.name,
      description: exercise.description,
      categories:
        typeof exercise.categories === 'string'
          ? [{ name: exercise.categories }]
          : exercise.categories,
      types:
        typeof exercise.types === 'string'
          ? [{ name: exercise.types }]
          : exercise.types,
    };
    console.log('dto', exercise);
    dispatch(
      exercisePutAction(exerciseDto, auth.user, handleSuccess, handleFailure)
    );
  };

  // Handle successful post
  const handleSuccess = () => {
    setToast({ show: true, message: 'Exercise updated' });
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to update exercise' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <div>
      <h1 className='page-heading'>Exercises</h1>
      <FloatRight>
        <BaseButton type='add' link='/add-new-exercise' text='Add exercise' />
      </FloatRight>
      <InfoTable
        columns={columns}
        data={exercises}
        expandedDetails={<ExerciseDetails />}
        onEdit={handleEdit}
        withFilter
      />

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(Exercises));
