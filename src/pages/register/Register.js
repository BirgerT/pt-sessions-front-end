import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import BaseButton from '../../components/baseButton/BaseButton.js'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import axios from 'axios';
import Toast from 'react-bootstrap/Toast'
import './Register.scss';
import config from '../../config.json';
import * as yup from 'yup';
import { Formik } from 'formik';

//eslint-disable-next-line
const gmailRegex = new RegExp('@gmail\.com$');
const RegistrationSchema = yup.object().shape({
  firstName: yup.string().required('Required'),
  lastName: yup.string().required('Required'),
  email: yup.string()
          .lowercase()
          .email('Invalid email')
          .matches(gmailRegex, 'Please enter a gmail address')
          .required('Required'),
  dob: yup.string().required('Required')
});


const RegisterTrainer = () => {
  const [messageVisible, setMessageVisible] = useState(false);
  const [messageText, setMessageText] = useState('Hello there sir!');

  async function register(values, { setSubmitting }) {
    setSubmitting(true);
    try {
      const resp = await axios.post(config.PT_SESS_API_BASE_URL + 'trainers', values);
      
      // All g, let's get out of here. (201 = Created)
      if (resp.status === 201) {
        window.location.href = config.PT_SESS_FRONT_BASE_URL + 'login';
        return;
      }
    }
    catch(error) {
      if (error.response.status === 400) {
        setMessageText("Email already registered.");
        setMessageVisible(true);
      }
      else {
        // Something terrible must have happened. 
        // Blame someone else and leave the user stranded.
        setMessageText("Uh, oh. The server doesn't like you very much... but you can try again later.")
      }
    }
    finally {
      setSubmitting(false);
    }
  };

  return (
    <div className="registerTrainer">
        <h1 className="page-heading">Register</h1>
        <Toast show={messageVisible} onClose={() => setMessageVisible(false)}>
          <Toast.Header>
            <strong>A most terrible error occurred!</strong>
          </Toast.Header>
          <Toast.Body>
            { messageText }
          </Toast.Body>
        </Toast>
        
        <Formik
          initialValues={{
            firstName: 'Jon',
            middleName: '',
            lastName: 'Langesønn',
            email: 'jon.langemann@gmail.com',
            address: 'Lengstegata 42',
            dob: '1956-11-15'
          }}
          validationSchema={RegistrationSchema}
          onSubmit={register}
        >
          {({ errors, touched, values, handleChange, handleSubmit, handleBlur, isSubmitting, isValid }) => (
            <Form aria-label="Trainer contact information" onSubmit={handleSubmit}>
            <Container>
              <Row>
                <Form.Group as={Col} >
                  <Form.Label>First name</Form.Label>
                  <Form.Control
                    type='text'
                    name='firstName'
                    value={values.firstName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={!!errors.firstName}
                    className={touched.firstName && errors.firstName ? 'error' : null}
                  />
                  <Form.Control.Feedback type='invalid'>
                    {errors.firstName}
                  </Form.Control.Feedback>
                </Form.Group>
                  <Form.Group as={Col} >
                    <Form.Label>Middle name</Form.Label>
                    <Form.Control
                      type='text'
                      name='middleName'
                      value={values.middleName}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={touched.middleName && errors.middleName ? 'error' : null}
                    />
                  </Form.Group>
                  <Form.Group as={Col} >
                    <Form.Label>Last name</Form.Label>
                    <Form.Control
                      type='text'
                      name='lastName'
                      value={values.lastName}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isInvalid={!!errors.lastName}
                      className={touched.lastName && errors.lastName ? 'error' : null}
                    />
                    <Form.Control.Feedback type='invalid'>
                    {errors.lastName}
                  </Form.Control.Feedback>
                  </Form.Group>
              </Row>
              <Row>
                <Form.Group as={Col} >
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type='email'
                    name='email'
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur} 
                    isInvalid={!!errors.email}
                    className={touched.email && errors.email ? 'error' : null}
                  />
                  <Form.Control.Feedback type='invalid'>
                    {errors.email}
                  </Form.Control.Feedback>
                </Form.Group>
              </Row>
              <Row>
                <Form.Group as={Col} >
                  <Form.Label>Address</Form.Label>
                  <Form.Control
                    type='text'
                    name='address'
                    value={values.address}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={touched.address && errors.address ? 'error' : null}
                  />
                </Form.Group>
              </Row>
              <Row>
                <Form.Group as={Col} >
                  <Form.Label>Date of birth</Form.Label>
                  <Form.Control 
                    type='date'
                    name='date'
                    value={values.dob}
                    onChange={handleChange}
                    onBlur={handleBlur} 
                    isInvalid={!!errors.dob}
                    className={touched.date && errors.date ? 'error' : null}
                  />
                  <Form.Control.Feedback type='invalid'>
                    {errors.dob}
                  </Form.Control.Feedback>
                </Form.Group>
              </Row>
              <Row>
                <Col><BaseButton type="Cancel" text="Cancel" link="/login" /></Col>
                <Col><BaseButton type="submit" text="Register" disabled={isSubmitting || !isValid} /></Col>
              </Row>
            </Container>
          </Form>
          )}
        </Formik>
    </div>
    );
}

export default RegisterTrainer;