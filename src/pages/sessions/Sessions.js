import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector, connect } from 'react-redux';
import {
  sessionsGetAction,
  sessionsPutAction,
} from '../../store/actions/sessionActions';
import { workoutsGetAction } from '../../store/actions/workoutActions';

import SessionDetails from './SessionDetails';
import InfoTable from '../../components/infoTable';
import BaseButton from '../../components/baseButton';
import FloatRight from '../../components/floatRight';
import SubmitToast from '../../components/submitToast';
import withAuth from '../../components/HOCs/auth/WithAuth';

const columns = [
  {
    name: 'Time',
    selector: 'time',
    sortable: true,
  },
  {
    name: 'Client',
    selector: 'clientName',
    sortable: true,
  },
  {
    name: 'Program',
    selector: 'program.name',
    sortable: true,
  },
  {
    name: 'Workouts',
    selector: 'workout.name',
    sortable: true,
  },
  {
    name: 'Done',
    selector: 'completed',
    sortable: true,
    cell: (row) => (
      <input type='checkbox' defaultChecked={row.done} onChange={(e) => {}} />
    ),
  },
];

function Sessions({ auth }) {
  const { sessions } = useSelector((state) => state.sessionReducer);
  const dispatch = useDispatch();

  const [toast, setToast] = useState({ show: false, message: '' });

  useEffect(() => {
    if (dispatch && auth)
      dispatch(sessionsGetAction(auth.user, handleFailedGet));
  }, [dispatch, auth]);

  const handleEdit = (data) => {
    dispatch(
      sessionsPutAction(
        JSON.parse(data.sessionData, auth.user, handleSuccess, handleFailure)
      )
    );

    // Terrible hacks below: since state isn't updated in Redux when posting,
    // we have to get the updated data back from the API. However, it takes
    // some time to POST, then it takes some time for the DB to update/insert.
    // So, gamble that it's done by ~2sec. If not, then we need a page refresh
    // to see changes.
    setTimeout(() => {
      dispatch(workoutsGetAction(auth.user, handleFailedGet));
      dispatch(sessionsGetAction(auth.user, handleFailedGet));
    }, 2000);
  };

  // Handle successful post
  const handleSuccess = () => {
    setToast({ show: true, message: 'Session updated' });
  };

  // Handle failed post
  const handleFailure = () => {
    setToast({ show: true, message: 'Failed to update session' });
  };

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <>
      <h1 className='page-heading'>Sessions</h1>
      <FloatRight>
        <BaseButton type='add' link='/add-new-session' text='Add session' />
      </FloatRight>
      <InfoTable
        columns={columns}
        data={sessions}
        expandedDetails={<SessionDetails />}
        onEdit={handleEdit}
      />

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(Sessions));
