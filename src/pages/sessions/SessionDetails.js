import React, { useEffect, useState } from 'react';
import InputField from '../../components/inputField/InputField.js';
import Form from 'react-bootstrap/Form';
import CustomSelect from '../../components/customSelect/CustomSelect.js';
import SubmitToast from '../../components/submitToast';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import withAuth from '../../components/HOCs/auth/WithAuth';
import { connect } from 'react-redux';

import { useDispatch, useSelector } from 'react-redux';
import { clientsGetAction } from '../../store/actions/clientActions';
import { venuesGetAction } from '../../store/actions/venueActions';
import { programsGetAction } from '../../store/actions/programActions';
import { workoutsGetAction } from '../../store/actions/workoutActions';

const idFromURL = (url) => parseInt(url.substring(url.lastIndexOf('/') + 1));

const SessionDetails = ({ data, disabled, onChange, auth }) => {
  const dispatch = useDispatch();
  const { clients } = useSelector((state) => state.clientReducer);
  const { venues } = useSelector((state) => state.venueReducer);
  const { programs } = useSelector((state) => state.programReducer);
  const { workouts } = useSelector((state) => state.workoutReducer);

  // Get all programs, all venues, and all workouts for each program
  const clientOptions = clients.map((c) => ({
    id: c.id,
    value: c.firstName + ' ' + c.lastName,
    label: c.firstName + ' ' + c.lastName,
  }));

  const mapIdName = (entity) => ({
    id: entity.id,
    value: entity.name + entity.id,
    label: entity.name,
  });

  const venueOptions = venues.map(mapIdName);

  const [workoutOptions, setWorkoutOptions] = useState({});
  const [programOptions, setProgramOptions] = useState({});

  const venueId = idFromURL(data.venue.url);
  const programId = idFromURL(data.program.url);
  const workoutId = idFromURL(data.workout.url);

  const [selectedClient, setSelectedClient] = useState(null);
  const [selectedVenue, setSelectedVenue] = useState({});
  const [selectedProgram, setSelectedProgram] = useState({});
  const [selectedWorkout, setSelectedWorkout] = useState({});

  const [toast, setToast] = useState({ show: false, message: '' });

  const cdate = new Date(data.time);
  const [time, setTime] = useState(
    ('0' + cdate.getHours()).slice(-2) +
      ':' +
      ('0' + cdate.getMinutes()).slice(-2)
  );

  // If we get new stuff from Redux, re-initialize state
  useEffect(() => {
    setWorkoutOptions(workouts.map(mapIdName));
    setProgramOptions(programs.map(mapIdName));

    setSelectedClient(
      clientOptions.filter((co) => co.value === data.clientName)[0]
    );
    setSelectedVenue(venueOptions.filter((vo) => vo.id === venueId)[0]);
    setSelectedProgram(
      programs.map(mapIdName).filter((po) => po.id === programId)[0]
    );
    setSelectedWorkout(
      workouts.map(mapIdName).filter((wo) => wo.id === workoutId)[0]
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [clients, venues, programs, workouts]);

  // Update programs, workouts to pick from when client updates
  useEffect(() => {
    if (!selectedClient) return;

    const newPrograms = programs.filter(
      (p) => idFromURL(p.client.url) === selectedClient.id
    );
    setProgramOptions(newPrograms.map(mapIdName));

    if (newPrograms.length === 0) setSelectedProgram({});
    else setSelectedProgram(newPrograms.map(mapIdName)[0]);

    const newWorkouts = newPrograms
      .flatMap((p) => p.workouts)
      .map((w) => ({ name: w.name, id: idFromURL(w.url) }))
      .map(mapIdName);
    setWorkoutOptions(newWorkouts);
    if (newWorkouts.length === 0) setSelectedWorkout({});
    else {
      // If the session's current workout is in this list, select it
      const existingWorkout = newWorkouts.filter(
        (wo) => wo.id === workoutId
      )[0];
      if (!existingWorkout) setSelectedWorkout(newWorkouts[0]);
      else setSelectedWorkout(existingWorkout);
    }

    //const newWorkouts = workouts.filter(w => idFromURL())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedClient]);

  // Manually dispatch fake events cuz.. reasons..
  useEffect(() => {
    if (!selectedProgram || !selectedWorkout || !selectedVenue) return;

    const fakeEvent = { target: {} };
    fakeEvent.target.name = 'sessionData';
    fakeEvent.target.value = JSON.stringify({
      id: data.id,
      time: data.time.split('T')[0] + 'T' + time,
      programId: selectedProgram.id,
      workoutId: selectedWorkout.id,
      venueId: selectedVenue.id,
      completed: data.completed,
    });

    onChange(fakeEvent);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedWorkout, selectedProgram, selectedVenue, time]);

  // Being one day before deadline, we(I) have degraded to shit practices and now just fetch EVERYTHING IN SIGHT
  useEffect(() => {
    if (dispatch && auth) {
      dispatch(clientsGetAction(auth.user, handleFailedGet));
      dispatch(venuesGetAction(auth.user, handleFailedGet));
      dispatch(programsGetAction(auth.user, handleFailedGet));
      dispatch(workoutsGetAction(auth.user, handleFailedGet));
    }
  }, [dispatch, auth]);

  // Handle failed get
  const handleFailedGet = () => {
    setToast({ show: true, message: 'Failed retrieving information from db' });
  };

  // Close the toast message
  const closeToast = () => {
    setToast({ show: false, message: '' });
  };

  return (
    <div>
      <Form>
        <Container>
          <Row>
            <Col>
              <InputField
                disabled={disabled}
                value={time}
                type='time'
                label={'Time'}
                placeholder={'Time'}
                onChange={setTime}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <CustomSelect
                disabled={disabled}
                label='Client'
                options={clientOptions}
                setSelectedOption={setSelectedClient}
                selectedOption={selectedClient}
              />
            </Col>
            <Col>
              <CustomSelect
                disabled={disabled}
                label='Venue'
                options={venueOptions}
                setSelectedOption={setSelectedVenue}
                selectedOption={selectedVenue}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <CustomSelect
                disabled={disabled}
                label='Program'
                options={programOptions}
                setSelectedOption={setSelectedProgram}
                selectedOption={selectedProgram}
              />
            </Col>
            <Col>
              <CustomSelect
                disabled={disabled}
                label='Workout'
                options={workoutOptions}
                setSelectedOption={setSelectedWorkout}
                selectedOption={selectedWorkout}
              />
            </Col>
          </Row>
        </Container>
      </Form>

      <SubmitToast
        message={toast.message}
        show={toast.show}
        setShow={closeToast}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default withAuth(connect(mapStateToProps, null)(SessionDetails));
