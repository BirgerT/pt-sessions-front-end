import React, { useState } from 'react'
import Logo from '../../assets/img/dumbbell.png'
import './Login.scss'
import { GoogleLogin } from 'react-google-login'
import { connect } from 'react-redux'
import { login } from '../../store/actions/authActions'
import { Redirect } from 'react-router-dom'
import config from '../../config.json'
import BaseButton from '../../components/baseButton/BaseButton'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

const Login = props =>  {
  const [errorMessage, setErrorMessage] = useState('Unable to login. Please register first.');
  const [showError, setShowError] = useState(false);
  const [isLoggingIn, setIsLoggingIn] = useState(false);

  const googleFailure = () => {};

  const googleResponse = resp => {
    const tokenBlob = new Blob([JSON.stringify({ TokenId: resp.tokenId }, null, 2)], { type: 'application/json' });
    const options = {
      method: 'POST',
      body: tokenBlob,
      mode: 'cors',
      cache: 'default'
    };

    // Azure goes to sleep after inactivity, so this may take some time. 
    // So, show loadspinner
    setIsLoggingIn(true);
    console.log('dialog, show!')

    // Send one-time code to backend, which (in)validates it
    // and returns a signed JWT token if successful.
    // We then dispatch the token to redux, and later append
    // it when accessing resources requiring authorization.
    fetch(config.GOOGLE_AUTH_CALLBACK_URL, options)
      .then(r => {
        if (!r.ok) {
          setShowError(true);
          return;
        }

        r.json().then(user => {
          const token = user.token;
          console.log('Logged in with token: ', token);
          props.login(token);
        });
      })
      .catch(err => {
        setErrorMessage('Something terrible occured when logging in. Please try again later.');
        setShowError(true);
        console.log('Error: ', err.message);
      })
      .finally(() => {
        setIsLoggingIn(false);
      });
  };

  if (!!props.auth.isAuthenticated) {
    return <Redirect to={{pathname: '/'}} />
  }

  return(
      <div className="login-page">
        <img src={Logo} alt="logo" className="login-logo"/>
        <h1 className="main-title">PT SESSIONS</h1>

        <div>
          { !isLoggingIn && 
            <GoogleLogin
              clientId={config.GOOGLE_CLIENT_ID}
              buttonText="Google Login"
              onSuccess={googleResponse}
              onFailure={googleFailure}
            />
          }
          { isLoggingIn && 
            <>
              <p className="loggingInText">Logging in...</p>
            </>
          }
        </div>
        <BaseButton 
          text={'Register'}
          link={'/register'}
          role={'button'}
          type={'registerBtn'}
        />

        <Modal show={showError} onHide={ () => setShowError(false) }>
          <Modal.Header closeButton>
            <Modal.Title>Login error</Modal.Title>
          </Modal.Header>
          <Modal.Body>{ errorMessage }</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={ () => setShowError(false) }>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
  );
};

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};
  
const mapDispatchToProps = dispatch => {
  return {
    login: token => {
      dispatch(login(token));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);