// Components
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Container from 'react-bootstrap/Container';
import Header from './components/header';

// Pages
import Dashboard from './pages/dashboard/Dashboard';
import Login from './pages/login/Login';
import Logout from './pages/logout/Logout';
import TrainerProfile from './pages/profile/TrainerProfile';
import Programs from './pages/programs/Programs';
import Workouts from './pages/workouts/Workouts';
import Exercises from './pages/exercises/Exercises';
import Sessions from './pages/sessions/Sessions';
import Clients from './pages/clients/Clients';
import Venues from './pages/venues/Venues';
import Trainers from './pages/trainers/Trainers';
import PersonalBests from './pages/personalBests/PersonalBests';

import AddNewProgram from './pages/addNewProgram/AddNewProgram';
import AddNewExercise from './pages/addNewExercise/AddNewExercise';
import AddNewSession from './pages/addNewSession/AddNewSession';
import AddNewClient from './pages/addNewClient/AddNewClient';
import AddNewVenue from './pages/addNewVenue/AddNewVenue';
import AddNewWorkout from './pages/addNewWorkout/AddNewWorkout';

import Register from './pages/register/Register';

import NotFound from './pages/notFound/NotFound';

// Style
import './utils/_variables.scss';
import './App.scss';

function App(props) {
  let containerClasses = 'app';
  if (!props.auth.isAuthenticated) {
    containerClasses += ' fullscreen';
  }
  return (
    <>
      <BrowserRouter basename='/pt-sessions-front-end'>
        <Header />

        <Container
          className={containerClasses}
          fluid={!props.auth.isAuthenticated}
        >
          <Switch>
            <Route exact path='/' component={Dashboard} />
            <Route path='/login' component={Login} />
            <Route path='/logout' component={Logout} />
            <Route path='/profile' component={TrainerProfile} />
            <Route path='/programs' component={Programs} />
            <Route path='/workouts' component={Workouts} />
            <Route path='/exercises' component={Exercises} />
            <Route path='/sessions' component={Sessions} />
            <Route exact path='/clients' component={Clients} />
            <Route path='/venues' component={Venues} />
            <Route path='/trainers' component={Trainers} />
            <Route path='/register' component={Register} />
            <Route exact path='/clients/:id/pbs' component={PersonalBests} />

            <Route path='/add-new-program' component={AddNewProgram} />
            <Route path='/add-new-exercise' component={AddNewExercise} />
            <Route path='/add-new-session' component={AddNewSession} />
            <Route path='/add-new-client' component={AddNewClient} />
            <Route path='/add-new-venue' component={AddNewVenue} />
            <Route path='/add-new-workout' component={AddNewWorkout} />
            <Route path='*' component={NotFound} />
          </Switch>
        </Container>
      </BrowserRouter>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

export default connect(mapStateToProps, null)(App);
