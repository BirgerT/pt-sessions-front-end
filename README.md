# PT Sessions
PT Sessions is an application that is responsible for handling the sessions trainers book with clients. These sessions include exercises that the trainer has decided the client should be doing. These sessions can be performed at many venues, including: gyms, homes, and public areas. This application is something that a trainer takes with them on the job and is used to track and manage their client’s progress.

## Team members and project participants
#### Team members
<ul>
    <li>Ole Hansson</li>
    <li>Birger Topphol</li>
    <li>Zakarias Laberg</li>
    <li>Camilla Arntzen</li>
</ul>

## Installation
<ul>
    <li>Clone the repository</li>
    <li>npm install</li>
    <li>npm start</li>
</ul>

